# Project README

This README provides an organized overview of the Java classes in the OMO smart_home project and highlights key functionalities as per specifications.

![Alt text](image-1.png)
![Alt text](image.png)

## Table of Contents

1. Entities
2. Device Control API
3. Device States
4. Appliance Consumption and Data Collection
5. Person and Animal Activities
6. Location and Events
7. Event Handling
8. Report Generation
9. Device Maintenance
10. Family and Leisure
11. Design Patterns

---

## F1. Entities

The project involves the following entities:

- House
- Window (with outdoor blinds)
- House Floor
- Sensor
- Device (Appliance)
- Person
- Non-agricultural Domestic Animals
- ...

---

## F2. Device Control API

Devices within the house have "APIs" for control.

Classes:

- cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity (the "API")

---

## F3. Device States

Devices have states that can be modified using the control API. Actions from the API depend on the device's current state.

Classes:

- cz.cvut.fel.omo.smart_home.model.device.Device
- cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext
- cz.cvut.fel.omo.smart_home.model.deviceState.DeviceState
- cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState
- cz.cvut.fel.omo.smart_home.model.deviceState.BrokenState
- cz.cvut.fel.omo.smart_home.model.deviceState.IdleState
- cz.cvut.fel.omo.smart_home.model.deviceState.OffState

---

## F4. Appliance Consumption and Data Collection

Appliances have consumption metrics in active state. Data is collected for electricity, gas, water consumption, and functionality over time.

Classes:

- cz.cvut.fel.omo.smart_home.model.device.api.Subscriber
- cz.cvut.fel.omo.smart_home.model.device.api.DataMeasureApi
- cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity
- cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData
- cz.cvut.fel.omo.smart_home.model.device.Device

---

## F5. Person and Animal Activities

Actors (persons and animals) can perform activities with effects on devices or other entities. For example, triggering a heater to turn on for it to later be either interacted with by another actor or to turn off by itself by the start of another iteration of the simulation.

Classes:

- cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity
- cz.cvut.fel.omo.smart_home.model.device.Device
- cz.cvut.fel.omo.smart_home.model.alive.personStrategy.JirkaStrategy
- cz.cvut.fel.omo.smart_home.model.alive.personStrategy.RepairmanStrategy
- cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy
- cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext
- cz.cvut.fel.omo.smart_home.model.deviceState.DeviceState
- cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState
- cz.cvut.fel.omo.smart_home.model.deviceState.BrokenState
- cz.cvut.fel.omo.smart_home.model.deviceState.IdleState
- cz.cvut.fel.omo.smart_home.model.deviceState.OffState

---

## F6. Location and Events

Devices and individuals are located in rooms (unless exercising) and generate random events. Events may convey important information or alerts, such as wind sensor triggering outdoor blind retraction.

Classes:

- cz.cvut.fel.omo.smart_home.model.alive.Alive
- cz.cvut.fel.omo.smart_home.model.house.Area
- cz.cvut.fel.omo.smart_home.repositories.AreaRepository
- cz.cvut.fel.omo.smart_home.model.event.visitor.VisitedElement (event related classes from here on below)
- cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor
- cz.cvut.fel.omo.smart_home.model.event.BreakDeviceEvent
- cz.cvut.fel.omo.smart_home.model.event.EventGenerator
- cz.cvut.fel.omo.smart_home.model.event.LossOfElectricityEvent
- cz.cvut.fel.omo.smart_home.model.event.StrongWindEvent
- cz.cvut.fel.omo.smart_home.model.event.SunriseEvent
- cz.cvut.fel.omo.smart_home.model.event.SunsetEvent
- cz.cvut.fel.omo.smart_home.model.event.WeakWindEvent

---

## F7. Event Handling

Events are handled by suitable individuals or devices. Examples include responding to a power outage by turning off non-essential appliances or addressing a water leak.

Classes: (I probably forgot to include some other ones which practically do the same.)

- cz.cvut.fel.omo.smart_home.model.event.LossOfElectricityEvent
- cz.cvut.fel.omo.smart_home.model.event.StrongWindEvent
- cz.cvut.fel.omo.smart_home.model.event.SunriseEvent
- cz.cvut.fel.omo.smart_home.model.event.SunsetEvent
- cz.cvut.fel.omo.smart_home.model.event.WeakWindEvent

---

## F8. Report Generation

Reports can be generated, including:

- **HouseConfigurationReport:** Configuration data preserving hierarchy - house -> floor -> room -> window -> blinds, etc. Also includes information about inhabitants.
- **HouseReport:** Actors (People, animals and devices) current position in correlation to the house configuration.
- **ActivityReport:** Actions and activities of individuals and animals, detailing device usage.
- **ConsumptionReport:** Consumption metrics (electricity, gas, water) for individual appliances, including financial estimates.

Classes:

- cz.cvut.fel.omo.smart_home.service.facade.ReportGeneratorFacade
- cz.cvut.fel.omo.smart_home.service.facade.houseReportFacade

---

## F9. Device Maintenance

In case of device malfunction, inhabitants must consult device documentation, find warranties, go through repair manuals, and perform corrective actions. Manuals are modeled as accessible variables in devices, retrieved when needed.

Classes:

- cz.cvut.fel.omo.smart_home.model.alive.personStrategy.RepairmanStrategy
- cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy
- cz.cvut.fel.omo.smart_home.model.device.strategy.ManualRepairStrategy
- cz.cvut.fel.omo.smart_home.model.device.strategy.RepairStrategy
- cz.cvut.fel.omo.smart_home.model.device.strategy.ReplaceRepairStrategy
- cz.cvut.fel.omo.smart_home.model.device.Device
- cz.cvut.fel.omo.smart_home.model.deviceState.DeviceState
- cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext
- cz.cvut.fel.omo.smart_home.model.deviceState.BrokenState

---

## 10. Family and Leisure

Our family is not even remotely active, therefore we've implemented a small percenteage of the family to be of ActiveStrategy, which goes out every now and then. And the rest of the family is just full of home-bodies.

Classes:

- cz.cvut.fel.omo.smart_home.model.alive.personStrategy.ActiveStrategy
- cz.cvut.fel.omo.smart_home.simulation.chain.AliveHandler

---

## F11. Design Patterns

Some of our design patterns have been implemented multiple times but to keep the document concise, we will only write out one example/implementation per design pattern.

Design Patterns and example Classes:

- Repository - cz.cvut.fel.omo.smart_home.repositories.AliveRepository
  ![Alt text](image-7.png)
- Strategy - cz.cvut.fel.omo.smart_home.model.device.strategy.ManualRepairStrategy
  ![Alt text](image-10.png)
- Proxy - cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy
  ![Alt text](image-6.png)
- State - cz.cvut.fel.omo.smart_home.model.deviceState.DeviceState
  ![Alt text](image-9.png)
- Visitor - cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor
  ![Alt text](image-11.png)
- Factory Method - cz.cvut.fel.omo.smart_home.model.factory.AliveFactory
  ![Alt text](image-4.png)
- Chain of Responsibility - cz.cvut.fel.omo.smart_home.simulation.chain.ChainOfResponsibility
  ![Alt text](image-2.png)
- Facade - cz.cvut.fel.omo.smart_home.service.facade.ReportGeneratorFacade
  ![Alt text](image-3.png)
- Singleton - cz.cvut.fel.omo.smart_home.utils.HouseConfigUtil
  ![Alt text](image-8.png)
- Observer -
  ![Alt text](image-5.png)
