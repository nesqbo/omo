package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.device.strategy.ReplaceRepairStrategy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.List;

/**
 * Represents a light device in the smart home system.
 */
@Slf4j
@Configurable
public class Light extends Device {

    /**
     * Creates a new instance of the Light device with the specified parameters.
     *
     * @param name           The name of the light.
     * @param subscribedApis The list of data measure APIs to which the light subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public Light(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.LIGHT, subscribedApis, repairProxy);
        this.setRepairStrategy(new ReplaceRepairStrategy());
    }

    @Override
    public void act() {
        if (this.isActive()) {
            increaseElectricityConsumption(10);
            decreaseCondition(10);
        }
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForLight(this);
    }
}
