package cz.cvut.fel.omo.smart_home.model.factory;

import cz.cvut.fel.omo.smart_home.model.house.Floor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Set;

/**
 * The {@code FloorFactory} class is responsible for creating instances of the {@code Floor} class.
 * It ensures that floors with unique numbers are created.
 */
@Slf4j
public class FloorFactory {
    private static final Set<Integer> floors = new HashSet<>();
    private FloorFactory() {
    }

    /**
     * Creates a new {@code Floor} instance with the specified floor number.
     *
     * @param floorNumber The floor number for the new floor.
     * @return A new instance of the {@code Floor} class.
     * @throws IllegalArgumentException If a floor with the given number already exists.
     */
    public static Floor createFloor(int floorNumber) {
        if (floors.contains(floorNumber)) {
            log.error("Floor with number [[{}]] already exists", floorNumber);
            throw new IllegalArgumentException(
                    String.format("Floor with number %d already exists", floorNumber));
        }
        floors.add(floorNumber);
        return new Floor(floorNumber);
    }
}
