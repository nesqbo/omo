package cz.cvut.fel.omo.smart_home.simulation.chain;

import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.utils.HouseConfigUtil;
import cz.cvut.fel.omo.smart_home.model.factory.RoomFactory;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import cz.cvut.fel.omo.smart_home.repositories.FloorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The {@code RoomHandler} class is responsible for handling the creation of rooms in the smart home simulation.
 */
@Component
@Slf4j
public class RoomHandler extends BaseHandler {

    private final AreaRepository roomRepository;
    private final FloorRepository floorRepository;
    private final HouseConfigUtil houseConfigUtil;
    @Autowired
    public RoomHandler(DeviceHandler deviceHandler, AreaRepository roomRepository, FloorRepository floorRepository, HouseConfigUtil houseConfigUtil) {
        super(deviceHandler);
        this.roomRepository = roomRepository;
        this.floorRepository = floorRepository;
        this.houseConfigUtil = houseConfigUtil;
    }

    @Override
    public void handle() {
        floorRepository.getAllFloors().forEach(floor -> {
            for (int i = 0; i < houseConfigUtil.roomCount; i++) {
                Room room = RoomFactory.createRoom(floor);
                roomRepository.addRoom(room);
            }
        });
        handleNext();
    }
}
