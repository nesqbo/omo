package cz.cvut.fel.omo.smart_home.repositories;

import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

/**
 * The {@code DataMeasureRepository} class manages the storage and retrieval of collectible data for devices in the smart home.
 */
@Repository
public class DataMeasureRepository {
    private HashMap<String, CollectibleData> devices = new HashMap<>();

    /**
     * Updates the collectible data for a specific device.
     *
     * @param deviceName The name of the device.
     * @param data       The collectible data to be updated.
     */
    public void update(String deviceName, CollectibleData data) {
        devices.put(deviceName, data);
    }

    /**
     * Retrieves the collectible data for a specific device.
     *
     * @param deviceName The name of the device.
     * @return The collectible data for the specified device.
     */
    public CollectibleData getDataForDevice(String deviceName) {
        return devices.get(deviceName);
    }

    /**
     * Retrieves all collectible data for devices.
     *
     * @return The map of devices and their associated collectible data.
     */
    public HashMap<String, CollectibleData> getAllData() {
        return devices;
    }
}
