package cz.cvut.fel.omo.smart_home.model.house;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Floor} class represents a floor within a smart home, containing rooms, outsides, and other areas.
 */
@Slf4j
public class Floor {
    @Getter
    private final int floorNumber;
    private final List<Area> areas;

    public Floor(int floorNumber) {
        this.floorNumber = floorNumber;
        this.areas = new ArrayList<>();
    }

    /**
     * Adds an area to the floor.
     *
     * @param area The area to be added.
     */
    public void addArea(Area area) {
        log.info("Adding area [{}] to floor [{}]", area.getName(), floorNumber);
        areas.add(area);
    }

    /**
     * Returns a list of rooms on the floor.
     *
     * @return The list of rooms.
     */
    public List<Room> getRooms() {
        return areas.stream().filter(area -> area instanceof Room).map(area -> (Room) area).toList();
    }

    /**
     * Returns a list of all areas on the floor.
     *
     * @return The list of all areas.
     */
    public List<Area> getAreas() {
        return List.copyOf(areas);
    }
}
