package cz.cvut.fel.omo.smart_home.model.alive.personStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.house.Area;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Abstract base class for defining strategies for interacting with devices and performing actions.
 */
@Component
@Scope("prototype")
@Slf4j
public abstract class BaseStrategy {
    @Autowired
    protected AliveRepository aliveRepository;

    @Autowired
    protected AreaRepository areaRepository;

    public BaseStrategy(){
    }

    /**
     * Initiates an interaction with the specified target entity.
     *
     * @param actor  The alive entity initiating the interaction.
     * @param target The entity to be interacted with.
     */
    public void interactWith(Alive actor, InteractibleEntity target){
        target.beInteractedWith();
    }

    /**
     * Abstract method for defining the actions to be performed by an alive entity.
     *
     * @param actor The alive entity performing the action.
     */
    public abstract void act(Alive actor);

    /**
     * Abstract method for handling interaction with an alive entity.
     */
    public abstract void beInteractedWith();

    /**
     * Retrieves the current area of the alive entity.
     *
     * @param alive The alive entity for which the area is retrieved.
     * @return The current area of the alive entity.
     */
    protected Area getCurrentArea(Alive alive){
        return areaRepository.findAreaByAliveName(alive.getName());
//        return areaRepository.findAreaByAliveName(alive.getName());
    }

    /**
     * Retrieves the current room of the alive entity.
     *
     * @param alive The alive entity for which the room is retrieved.
     * @return The current room of the alive entity.
     */
    protected Room getCurrentRoom(Alive alive){
        return areaRepository.findRoomByAliveName(alive.getName());
    }
}