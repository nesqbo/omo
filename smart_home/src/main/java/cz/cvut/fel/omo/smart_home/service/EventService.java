package cz.cvut.fel.omo.smart_home.service;

import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.event.BreakDeviceEvent;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * The {@code EventService} class is a service responsible for handling events related to devices in the smart home.
 * It is annotated with {@link Service} to indicate that it is a Spring service component.
 * The service provides methods to trigger events, such as breaking a device.
 * The class is dependent on {@link DeviceRepository} and {@link AliveRepository}.
 */
@Service
public class EventService {

    private final DeviceRepository deviceRepository;

    private final AliveRepository aliveRepository;

    @Autowired
    public EventService(DeviceRepository deviceRepository, AliveRepository aliveRepository) {
        this.deviceRepository = deviceRepository;
        this.aliveRepository = aliveRepository;
    }

    /**
     * Breaks the specified device by triggering a {@link BreakDeviceEvent}.
     *
     * @param deviceName The name of the device to be broken.
     */
    public void breakDevice(String deviceName) {
        Device deviceToBreak = deviceRepository.getDeviceByDeviceName(deviceName);
        BreakDeviceEvent breakDeviceEvent = new BreakDeviceEvent(deviceRepository, aliveRepository);
        breakDeviceEvent.visit(deviceToBreak.getDeviceInteractionAPI());
    }
}
