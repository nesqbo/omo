package cz.cvut.fel.omo.smart_home.model.event.visitor;

/**
 * Interface for elements that can be visited by a Visitor.
 */
public interface VisitedElement {

    /**
     * Accepts a Visitor, allowing it to perform operations on the visited element.
     *
     * @param visitor The Visitor to be accepted.
     */
    void accept(Visitor visitor);
}
