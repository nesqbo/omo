package cz.cvut.fel.omo.smart_home.model.house;


import cz.cvut.fel.omo.smart_home.model.device.OutdoorBlinds;

/**
 * The {@code Window} class represents a window in a room, which may have associated outdoor blinds.
 */
public class Window {
    private OutdoorBlinds outdoorBlinds;

    /**
     * Sets the outdoor blinds for the window.
     *
     * @param outdoorBlinds The outdoor blinds to be associated with the window.
     * @throws RuntimeException If outdoor blinds are already set for the window.
     */
    public void setOutdoorBlinds(OutdoorBlinds outdoorBlinds) {
        if (this.outdoorBlinds != null) {
            throw new RuntimeException("Outdoor blinds already set");
        }
        this.outdoorBlinds = outdoorBlinds;
    }
}
