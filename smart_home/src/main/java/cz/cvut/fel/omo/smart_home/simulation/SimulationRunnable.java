package cz.cvut.fel.omo.smart_home.simulation;

import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import cz.cvut.fel.omo.smart_home.service.facade.ReportGeneratorFacade;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import cz.cvut.fel.omo.smart_home.simulation.chain.AliveHandler;
import cz.cvut.fel.omo.smart_home.model.event.EventGenerator;
import cz.cvut.fel.omo.smart_home.model.event.visitor.VisitedElement;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The {@code SimulationRunnable} class represents the main execution logic for the simulation. It implements the {@link Runnable} interface,
 * allowing it to be run on a separate thread. The simulation involves iterating over a list of {@link SimulationActor}s, including alive entities and devices,
 * and triggering their actions. Additionally, random events are generated during each iteration. The simulation can be configured with the maximum number of iterations
 * and the time between iterations.
 */
@Slf4j
@Component
public class SimulationRunnable implements Runnable {
    List<SimulationActor> simulationActors;
    SimulationIterator simulationActorIterator;
    AliveRepository aliveRepository;
    AliveHandler aliveHandler;
    DeviceRepository deviceRepository;
    private long timeBetweenIterations = 0;
    private int maxIterationCount = 0;
    private final EventGenerator eventGenerator;
    private final ReportGeneratorFacade reportGeneratorFacade;

    @Autowired
    public SimulationRunnable(AliveRepository aliveRepository, DeviceRepository deviceRepository, AliveHandler aliveHandler, EventGenerator eventGenerator, ReportGeneratorFacade reportGeneratorFacade) {
        this.aliveRepository = aliveRepository;
        this.deviceRepository = deviceRepository;
        this.aliveHandler = aliveHandler;
        this.eventGenerator = eventGenerator;
        this.reportGeneratorFacade = reportGeneratorFacade;
    }

    /**
     * Sets up the simulation with the specified configuration for iteration count and time between iterations.
     *
     * @param configIterationCount the maximum number of iterations for the simulation.
     * @param configBetweenIterations the time between iterations in milliseconds.
     */
    public void setup(int configIterationCount, long configBetweenIterations){
        this.maxIterationCount = configIterationCount;
        this.timeBetweenIterations = configBetweenIterations;

        List<SimulationActor> temp = new ArrayList<>();
        temp.addAll(aliveRepository.getAlives().values());
        temp.addAll(deviceRepository.getAllDevices().stream().map(Device::getDeviceInteractionAPI).toList());
        simulationActors = Collections.synchronizedList(temp);
        simulationActorIterator = new SimulationIterator(this.simulationActors);
        simulationIteration = 0;
    }
    private int simulationIteration;

    @Override
    public void run() {
        if (simulationActors.isEmpty()){
            throw new SimulationException("No simulation actors. Please add a human or device to the house.");
        }
        long nextIterTime = System.currentTimeMillis() + timeBetweenIterations;
        while (simulationIteration < maxIterationCount) {
            if (System.currentTimeMillis() > nextIterTime) {
                nextIterTime = System.currentTimeMillis() + timeBetweenIterations;
                log.info("================================ Simulation iteration [{}] ================================", ++simulationIteration);
                allSimulationActorsAct();
                log.info("\n");
                log.info("================================ Events ================================");
                randomEvent();
                log.info("\n\n\n\n");
            }
        }
        reportGeneratorFacade.generateDeviceReport(deviceRepository.getRandomDeviceOfType(DeviceType.CIRCUIT_BREAKER).getDeviceInteractionAPI().getName());
        reportGeneratorFacade.writeToFile(reportGeneratorFacade.generateConsumptionReport(), ReportGeneratorFacade.ReportType.CONSUMPTION);

        log.info("================================ House Report ================================");
        log.info("\n" + reportGeneratorFacade.generateHouseReport().toString(2));
        reportGeneratorFacade.writeToFile(reportGeneratorFacade.generateHouseReport(), ReportGeneratorFacade.ReportType.HOUSE);


        log.info("================================ Alive Report ================================");
        log.info("\n" + reportGeneratorFacade.generateAliveReport().toString(2));
        reportGeneratorFacade.writeToFile(reportGeneratorFacade.generateAliveReport(), ReportGeneratorFacade.ReportType.ALIVES);

        log.info("================================ END OF SIMULATION ================================");

//        System.exit(0);
    }

    private void allSimulationActorsAct() {
        synchronized(simulationActors) {
            do {
                simulationActorIterator.current().act();
                simulationActorIterator.next();
            } while (simulationActorIterator.hasNext());
            simulationActorIterator.reset();
        }
    }

    private void randomEvent() {
        Visitor randomEvent = eventGenerator.generateRandomEvent();
        VisitedElement randomElement = randomEvent.getRandomCompatibleElement();
        randomEvent.visit(randomElement);
    }
}
