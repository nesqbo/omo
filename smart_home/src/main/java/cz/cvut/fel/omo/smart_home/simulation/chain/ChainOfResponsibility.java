package cz.cvut.fel.omo.smart_home.simulation.chain;

public class ChainOfResponsibility {
    private Handler rootHandler;

    public ChainOfResponsibility(Handler rootHandler) {
        this.rootHandler = rootHandler;
    }

    public void startChain() {
        rootHandler.handle();
    }
}
