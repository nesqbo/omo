package cz.cvut.fel.omo.smart_home.model.alive.animalStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.alive.Animal;
import cz.cvut.fel.omo.smart_home.model.alive.Person;

import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.BaseStrategy;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.house.Area;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Scope("prototype")
public class CatStrategy extends BaseStrategy {
    @Override
    public void interactWith(Alive actor, InteractibleEntity target) {
        log.info("Cat [{}]: Meow meow meow. I want to cuddle.", actor.getName());
        target.beInteractedWith();
    }

    @Override
    public void act(Alive animal) {
        Area room = getCurrentArea(animal);

        if (!room.getPeople().isEmpty()) {
            Person a = room.getPeople().get(0);
            log.info("Cat [{}]: I am a cat. I am [{}] human [{}].", animal.getName(), getCatPlayType(), a.getName());
            a.beInteractedWith();
        } else if (room.getAnimals().size() >= 2){
            for (Animal a : room.getAnimals()){
                if (a != animal){
                    log.info("Cat [{}]: I am a cat. I am playing with my animal friend [{}].", animal.getName(), a.getName());
                    a.beInteractedWith();
                    break;
                }
            }
        }else {
            log.info("Cat [{}]: I am a cat. I am doing cat things.", animal.getName());
        }
    }

    @Override
    public void beInteractedWith() {
        log.info("Cat: Meow meow meow. Nyaruhodo.");
    }

    private String getCatPlayType(){
        int random = (int) (Math.random() * catPlayType.values().length);
        return catPlayType.values()[random].toString();
    }

    public enum catPlayType{
        SCRATCHING,
        CUDDLING,
        BITING,
        PLAYING_WITH,
        SLEEPING_WITH,
        EATING_WITH
    }
}
