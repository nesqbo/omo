package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a light sensor device in the smart home system.
 */
@Getter
@Slf4j
public class LightSensor extends Device {
    //imagine like a hue bridge has lights it manages
    private final List<Light> lights = new ArrayList<>();

    /**
     * Creates a new instance of the LightSensor device with the specified parameters.
     *
     * @param name           The name of the light sensor.
     * @param subscribedApis The list of data measure APIs to which the light sensor subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public LightSensor(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.LIGHT_SENSOR, subscribedApis, repairProxy);
        this.getDeviceInteractionAPI().setState(new ActiveState());
    }

    /**
     * Adds a list of lights to be managed by the light sensor.
     *
     * @param lights The list of lights to add.
     */
    public void addLights(List<Light> lights) {
        this.lights.addAll(lights);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForLightSensor(this);
    }

    @Override
    public void act() {
        if (state.getStateName().equals("Active")) {
            increaseElectricityConsumption(10);
        }
    }
}
