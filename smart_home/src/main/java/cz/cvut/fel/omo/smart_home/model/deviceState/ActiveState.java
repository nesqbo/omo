package cz.cvut.fel.omo.smart_home.model.deviceState;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ActiveState extends DeviceState{
    public ActiveState(int electricityConsumption, int gasConsumption, int waterConsumption, int conditionDecrease) {
        super(electricityConsumption, gasConsumption, waterConsumption, conditionDecrease);
        stateName = "Active";
    }

    public ActiveState() {
        super(10, 0, 0, 10);
        stateName = "Active";
    }

    @Override
    public void changeToNextState(DeviceContext context) {
        DeviceState newState = context.getDeviceStates().get("Idle");
        context.setState(newState);
    }

}
