package cz.cvut.fel.omo.smart_home.model.alive.animalStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.alive.Animal;
import cz.cvut.fel.omo.smart_home.model.alive.Person;

import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.BaseStrategy;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.house.Area;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Scope("prototype")
public class DogStrategy extends BaseStrategy {
    @Override
    public void interactWith(Alive actor, InteractibleEntity target) {
        actor.interactWith(target);
        log.info("Dog [{}]: I am interacting with [{}] in room [{}]", actor.getName(), target.getName(), getCurrentArea(actor).getName());
        target.beInteractedWith();
    }

    @Override
    public void act(Alive animal) {
        Area room = getCurrentArea(animal);

        if (room.getAnimals().size() >= 2){
            for (Animal a : room.getAnimals()){
                if (a != animal){
                    log.info("Dog [{}]: I am a dog. I am doing dog things with my animal friend [{}].", animal.getName(), a.getName());
                    animal.beInteractedWith();
                    break;
                }
            }
        } else if (!room.getPeople().isEmpty()){
            Person a = room.getPeople().get(0);
            log.info("Dog [{}]: I am a dog. I am doing dog things with a human [{}].", animal.getName(), a.getName());
            a.beInteractedWith();
        } else {
            log.info("Dog [{}]: I am a dog. I am doing dog things.", animal.getName());
        }
    }

    @Override
    public void beInteractedWith() {
        log.info("Dog: Yayyyy we're playing. Woof woof.");
    }
}
