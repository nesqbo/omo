package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState;
import cz.cvut.fel.omo.smart_home.model.deviceState.IdleState;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Represents a gas stove device in the smart home system.
 */
@Slf4j
public class GasStove extends Device {

    /**
     * Creates a new instance of the GasStove device with the specified parameters.
     *
     * @param name           The name of the gas stove.
     * @param subscribedApis The list of data measure APIs to which the gas stove subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public GasStove(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.GAS_STOVE, subscribedApis, repairProxy);
        setActiveState(new ActiveState(0, 30, 0, 10));
        setIdleState(new IdleState(0, 10, 0, 5));
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForGasStove(this);
    }

    @Override
    public void act() {
        if (isBroken()) {
            return;
        }
        if (isActive()) {
            increaseGasConsumption(30);
            decreaseCondition(10);
        }
    }
}
