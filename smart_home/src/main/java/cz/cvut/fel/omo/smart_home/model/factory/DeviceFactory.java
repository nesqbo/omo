package cz.cvut.fel.omo.smart_home.model.factory;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;

import java.util.List;

/**
 * Factory class for creating instances of devices in the smart home system.
 */
public class DeviceFactory {
    private DeviceFactory() {
    }

    /**
     * Creates a device of the specified type with the given parameters.
     *
     * @param <T>            The type of the device.
     * @param type           The type of the device to be created.
     * @param name           The name of the device.
     * @param subscribedApis The list of data measure APIs to which the device subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     * @return A new instance of the specified device type.
     * @throws IllegalArgumentException If the provided device type is null or not supported.
     */
    public static <T extends Device> T createDevice(DeviceType type, String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        if (type != null) {
            return (T) type.createDevice(name, subscribedApis, repairProxy);
        }
        throw new IllegalArgumentException("Device type not supported");
    }
}
