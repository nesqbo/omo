package cz.cvut.fel.omo.smart_home.model.device.collectibleData;

import cz.cvut.fel.omo.smart_home.model.deviceState.BrokenState;
import cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext;
import cz.cvut.fel.omo.smart_home.model.deviceState.OffState;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Class representing collectible data for a device in the smart home system.
 */
@Slf4j
@Getter
public class CollectibleData {
    private double electricity = 0;
    private double water = 0;
    private double gas = 0;
    private double condition = 100;

    /**
     * Adds the given amount of electricity consumption to the collected data.
     *
     * @param electricity The amount of electricity consumption to add.
     */
    public void addElectricity(double electricity) {
        if (electricity < 0) {
            log.error("Electricity cannot be negative");
            return;
        }
        this.electricity += electricity;
    }

    /**
     * Adds the given amount of water consumption to the collected data.
     *
     * @param water The amount of water consumption to add.
     */
    public void addWater(double water) {
        if (water < 0) {
            log.error("Water cannot be negative");
            return;
        }
        this.water += water;
    }

    /**
     * Adds the given amount of gas consumption to the collected data.
     *
     * @param gas The amount of gas consumption to add.
     */
    public void addGas(double gas) {
        if (gas < 0) {
            log.error("Gas cannot be negative");
            return;
        }
        this.gas += gas;
    }

    /**
     * Decreases the device's condition by the specified amount and updates its state.
     *
     * @param conditionDecrease The amount by which the condition should be decreased.
     * @param context           The context of the device to manage its state.
     */
    public void decreaseCondition(double conditionDecrease, DeviceContext context) {
        if (condition == 0) {
            return;
        }
        if (conditionDecrease < 0) {
            log.error("Condition cannot be negative");
            return;
        }
        if (this.condition - conditionDecrease < 0) {
            this.condition = 0;
            log.info("Device is now broken");
            context.setState(new BrokenState());
            return;
        }
        this.condition -= conditionDecrease;
    }

    /**
     * Restores the device's condition by the specified amount and updates its state.
     *
     * @param restoreBy The amount by which the condition should be restored.
     * @param context   The context of the device to manage its state.
     */
    public void restoreCondition(int restoreBy, DeviceContext context) {
        log.info("Restoring condition of device [{}] by [{}]", context.getName(), restoreBy);
        this.condition += restoreBy;
        if (context.getState() instanceof BrokenState && this.condition > 0) {
            log.info("Device is now repaired");
            context.setState(new OffState());
        }
        if (this.condition > 100) {
            this.condition = 100;
        }
    }
}
