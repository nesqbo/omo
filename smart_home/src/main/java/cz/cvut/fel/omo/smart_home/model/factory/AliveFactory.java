package cz.cvut.fel.omo.smart_home.model.factory;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.alive.Animal;
import cz.cvut.fel.omo.smart_home.model.alive.Person;
import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.BaseStrategy;

import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * A factory class for creating instances of Alive entities in the smart home system.
 */
public class AliveFactory {
    public static Random random = new Random();
    private AliveFactory() {
    }

    /**
     * Creates a new instance of an Alive entity based on the specified type, name, and strategies.
     *
     * @param type       The type of the Alive entity.
     * @param name       The name of the Alive entity.
     * @param strategies The list of strategies associated with the Alive entity.
     * @param <T>        The type of Alive entity to create (Person or Animal).
     * @return A new instance of the specified Alive entity type.
     * @throws IllegalArgumentException if the Alive type is not supported.
     */
    public static <T extends Alive> T createAlive(Alive.AliveType type, String name, List<BaseStrategy> strategies) {

        BaseStrategy strategy;

        int num = random.nextInt(strategies.size());
        strategy = strategies.get(num);
        if (Objects.requireNonNull(type) == Alive.AliveType.PERSON) {
            return (T) new Person(name, strategy);
        }
        if (Objects.requireNonNull(type) == Alive.AliveType.ANIMAL) {
            return (T) new Animal(name, strategy);
        }
        throw new IllegalArgumentException("Alive type not supported");
    }
}
