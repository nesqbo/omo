package cz.cvut.fel.omo.smart_home.model.event.visitor;

import cz.cvut.fel.omo.smart_home.model.device.WashingMachine;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.Heater;
import cz.cvut.fel.omo.smart_home.model.device.Light;
import cz.cvut.fel.omo.smart_home.model.device.LightSensor;
import cz.cvut.fel.omo.smart_home.model.device.OutdoorBlinds;
import cz.cvut.fel.omo.smart_home.model.device.WindSensor;
import cz.cvut.fel.omo.smart_home.model.device.CircuitBreaker;
import cz.cvut.fel.omo.smart_home.model.device.GasStove;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Represents an abstract visitor for devices in the smart home system.
 */
public abstract class Visitor {
    protected final DeviceRepository deviceRepository;

    @Autowired
    public Visitor(DeviceRepository deviceRepository, AliveRepository aliveRepository) {
        this.deviceRepository = deviceRepository;
    }

    /**
     * Visits the specified element using the appropriate visit method.
     *
     * @param visitedElement The element being visited.
     */
    public void visit(VisitedElement visitedElement) {
        visitedElement.accept(this);
    }

    /**
     * Performs the visit operation for a generic device. This method is overridden by specific device visit methods.
     *
     * @param device The generic device.
     */
    public void doForDevice(Device device) {
        throw new UnsupportedOperationException("This event cannot be applied to devices.");
    }

    /**
     * Performs the visit operation for a light device. This method is overridden by specific light device visit methods.
     *
     * @param light The light device.
     */
    public void doForLight(Light light) {
        throw new UnsupportedOperationException("This event cannot be applied to lights.");
    }

    /**
     * Performs the visit operation for a light sensor device. This method is overridden by specific light sensor device visit methods.
     *
     * @param lightSensor The light sensor device.
     */
    public void doForLightSensor(LightSensor lightSensor) {
        throw new UnsupportedOperationException("This event cannot be applied to light sensors.");
    }

    /**
     * Abstract method to get a random compatible element for visitation.
     *
     * @return A randomly selected compatible element.
     */
    public abstract VisitedElement getRandomCompatibleElement();

    //specific visit methods for each device type
    public void doForHeater(Heater heater) {
        throw new UnsupportedOperationException("This event cannot be applied to heaters.");
    }

    public void doForWindSensor(WindSensor windSensor) {
        throw new UnsupportedOperationException("This event cannot be applied to wind sensors.");
    }

    public void doForOutdoorBlinds(OutdoorBlinds outdoorBlinds) {
        throw new UnsupportedOperationException("This event cannot be applied to outdoor blinds.");
    }

    public void doForCircuitBreaker(CircuitBreaker circuitBreaker) {
        throw new UnsupportedOperationException("This event cannot be applied to circuit breakers.");
    }

    public void doForGasStove(GasStove gasStove) {
        throw new UnsupportedOperationException("This event cannot be applied to gas stoves.");
    }

    public void doForWashingMachine(WashingMachine washingMachine) {
        throw new UnsupportedOperationException("This event cannot be applied to washing machines.");
    }
}
