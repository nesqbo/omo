package cz.cvut.fel.omo.smart_home.utils;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * The {@code SimulationConfigUtil} class is responsible for loading and validating simulation configuration properties.
 * Configuration properties are loaded from the "simulation-configuration.properties" file.
 */
@Configuration
@PropertySource("classpath:simulation-configuration.properties")
public class SimulationConfigUtil{

    /**
     * The maximum number of iterations for the simulation.
     */
    @Value("${simulation.max-iterations-count}")
    public int maxIterationsCount;

    /**
     * The minimum delay between iterations in the simulation.
     */
    @Value("${simulation.min-delay}")
    public int iterationDelay;

    /**
     * Validates the loaded configuration properties after the bean is constructed.
     * It checks whether the max iterations count and iteration delay are valid.
     * If not, it throws an IllegalArgumentException.
     */
    @PostConstruct
    public void validate(){
        if(maxIterationsCount <= 0){
            throw new IllegalArgumentException("Invalid max iterations count: " + maxIterationsCount);
        }

        if(iterationDelay <= 0){
            throw new IllegalArgumentException("Invalid iteration delay: " + iterationDelay);
        }
    }

}