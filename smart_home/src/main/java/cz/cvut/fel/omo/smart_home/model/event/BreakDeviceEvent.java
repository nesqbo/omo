package cz.cvut.fel.omo.smart_home.model.event;

import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.Light;
import cz.cvut.fel.omo.smart_home.model.device.LightSensor;
import cz.cvut.fel.omo.smart_home.model.device.WashingMachine;
import cz.cvut.fel.omo.smart_home.model.device.Heater;
import cz.cvut.fel.omo.smart_home.model.device.OutdoorBlinds;
import cz.cvut.fel.omo.smart_home.model.device.GasStove;
import cz.cvut.fel.omo.smart_home.model.device.WindSensor;
import cz.cvut.fel.omo.smart_home.model.device.CircuitBreaker;
import cz.cvut.fel.omo.smart_home.model.event.visitor.VisitedElement;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.Random;

@Slf4j
@Configurable
public class BreakDeviceEvent extends Visitor {
    @Autowired
    public BreakDeviceEvent(DeviceRepository deviceRepository, AliveRepository aliveRepository) {
        super(deviceRepository, aliveRepository);
    }

    public void doForDevice(Device device) {
        Random random = new Random();
        int randomInt = random.nextInt(1, 5)*10;
        device.getDeviceInteractionAPI().decreaseCondition(randomInt);
        log.info("Device [{}] is defective and it's condition decreased.", device.getDeviceInteractionAPI().getName());
    }

    @Override
    public void doForLight(Light light) {
        doForDevice(light);
    }

    @Override
    public void doForLightSensor(LightSensor lightSensor) {
        doForDevice(lightSensor);
    }

    @Override
    public VisitedElement getRandomCompatibleElement() {
        return deviceRepository.getRandomDevice().getDeviceInteractionAPI();
    }

    @Override
    public void doForHeater(Heater heater) {
        doForDevice(heater);
    }

    @Override
    public void doForWindSensor(WindSensor windSensor) {
        doForDevice(windSensor);
    }

    @Override
    public void doForOutdoorBlinds(OutdoorBlinds outdoorBlinds) {
        doForDevice(outdoorBlinds);
    }

    @Override
    public void doForCircuitBreaker(CircuitBreaker circuitBreaker) {
        doForDevice(circuitBreaker);
    }

    @Override
    public void doForGasStove(GasStove gasStove) {
        doForDevice(gasStove);
    }

    @Override
    public void doForWashingMachine(WashingMachine washingMachine) {
        doForDevice(washingMachine);
    }
}
