package cz.cvut.fel.omo.smart_home.repositories;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.house.Area;
import cz.cvut.fel.omo.smart_home.model.house.Floor;
import cz.cvut.fel.omo.smart_home.model.house.Outside;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;

/**
 * The {@code AreaRepository} class manages the storage and retrieval of rooms, outsides, and other areas in the smart home.
 */
@Repository
@Slf4j
public class AreaRepository {
    private static final HashMap<String, Room> rooms = new HashMap<>();
    private static final HashMap<String, Outside> outsides = new HashMap<>();
    private static final HashMap<String, Area> areas = new HashMap<>();
    private final Random random = new Random();

    /**
     * Adds a room to the repository.
     *
     * @param room The room to be added.
     * @throws IllegalArgumentException If a room with the same name already exists.
     */
    public void addRoom(Room room) {
        if (areas.containsKey(room.getName()) || rooms.containsKey(room.getName())) {
            log.error("Room with name [[{}]] already exists", room.getName());
            throw new IllegalArgumentException(
                    String.format("Room with name %s already exists", room.getName()));
        }
        rooms.put(room.getName(), room);
        areas.put(room.getName(), room);
    }

    /**
     * Retrieves a room from the repository based on the name of a device in the room.
     *
     * @param deviceName The name of the device in the room.
     * @return The room containing the specified device.
     * @throws IllegalArgumentException If no room with the specified device name exists.
     */
    public Room getRoomByDeviceName(String deviceName) {
        for (Room room : rooms.values()) {
            for (Device device : room.getDevices()) {
                if (device.getDeviceInteractionAPI().getName().equals(deviceName)) {
                    return room;
                }
            }
        }
        log.error("Device with name [[{}]] does not exist", deviceName);
        throw new IllegalArgumentException(String.format("Device with name %s does not exist", deviceName));
    }

    /**
     * Returns a collection of all rooms in the repository.
     *
     * @return The collection of rooms.
     */
    public Collection<Room> getRooms() {
        return rooms.values();
    }

    /**
     * Finds a room in the repository based on the name of a living being in the room.
     *
     * @param aliveName The name of the living being in the room.
     * @return The room containing the specified living being.
     * @throws IllegalStateException If no rooms are found for the specified living being.
     */
    public Room findRoomByAliveName(String aliveName) {
        for (Room room : rooms.values()) {
            if (room.getPeople().stream().anyMatch(person -> person.getName().equals(aliveName))) {
                return room;
            }
            if (room.getAnimals().stream().anyMatch(animal -> animal.getName().equals(aliveName))) {
                return room;
            }
        }
        throw new IllegalStateException(String.format("No rooms found for alive %s", aliveName));
    }

    /**
     * Finds a room in the repository with a broken device.
     *
     * @return An optional containing the room with a broken device, or empty if no such room is found.
     */
    public Optional<Room> findRoomWithBrokenDevice() {
        for (Room room : rooms.values()) {
            if (room.getDevices().stream().anyMatch(Device::isBroken)) {
                return Optional.of(room);
            }
        }
        return Optional.empty();
    }

    /**
     * Returns a random room from the repository.
     *
     * @return A randomly selected room.
     * @throws IllegalStateException If no rooms are found.
     */
    public Room getRandomRoom() {
        return rooms.values().stream().skip(random.nextInt(rooms.size())).findFirst().orElseThrow(() -> new IllegalStateException("No rooms found"));
    }

    /**
     * Adds an outside area to the repository.
     *
     * @param outside The outside area to be added.
     * @throws IllegalArgumentException If an outside area with the same name already exists.
     */
    public void addOutside(Outside outside) {
        if (outsides.containsKey(outside.getName()) || areas.containsKey(outside.getName())) {
            log.error("Outside with name [[{}]] already exists", outside.getName());
            throw new IllegalArgumentException(
                    String.format("Outside with name %s already exists", outside.getName()));
        }
        outsides.put(outside.getName(), outside);
        areas.put(outside.getName(), outside);
    }

    /**
     * Returns a randomly selected outside area from the repository.
     *
     * @return A randomly selected outside area.
     * @throws IllegalStateException If no outside areas are found.
     */
    public Outside getRandomOutside(){
        return outsides.values().stream().skip(random.nextInt(outsides.size())).findFirst().orElseThrow(() -> new IllegalStateException("No outsides found"));
    }

    /**
     * Creates and adds a new outside area to the repository on the specified floor with the given name.
     *
     * @param floor The floor to which the outside area belongs.
     * @param name  The name of the outside area.
     */
    public void createOutside(Floor floor, String name){
        Outside outside = new Outside(floor, name + random.nextInt(1000));
        addOutside(outside);
    }

    /**
     * Finds an area in the repository based on the name of a living being in the area.
     *
     * @param aliveName The name of the living being in the area.
     * @return The area containing the specified living being.
     * @throws IllegalStateException If no areas are found for the specified living being.
     */
    public Area findAreaByAliveName(String aliveName){


        for (Area area : areas.values()) {
            if (area.getPeople().stream().anyMatch(person -> person.getName().equals(aliveName))) {
                return area;
            }
            if (area.getAnimals().stream().anyMatch(animal -> animal.getName().equals(aliveName))) {
                return area;
            }
        }
        throw new IllegalStateException(String.format("No areas found for alive %s", aliveName));
    }

    /**
     * Returns a list of devices in a room based on the room name.
     *
     * @param roomName The name of the room.
     * @return The list of devices in the specified room.
     */
    public List<Device> getDevicesByRoomName(String roomName) {
        List<Device> devices = new ArrayList<>();
        for (Room room : rooms.values()) {
            if (room.getName().equals(roomName)) {
                devices.addAll(room.getDevices());
            }
        }
        return devices;
    }

    /**
     * Returns a collection of all outside areas in the repository.
     *
     * @return The collection of outside areas.
     */
    public Collection<Outside> getOutsides() {
        return outsides.values();
    }

    /**
     * Returns a list of living beings in a room based on the room name.
     *
     * @param name The name of the room.
     * @return The list of living beings in the specified room.
     */
    public List<Alive> getAlivesByRoomName(String name) {
        List<Alive> alives = new ArrayList<>();
        for (Room room : rooms.values()) {
            if (room.getName().equals(name)) {
                alives.addAll(room.getPeople());
                alives.addAll(room.getAnimals());
            }
        }
        return alives;
    }

    /**
     * Returns a list of living beings in an outside area based on the outside area name.
     *
     * @param name The name of the outside area.
     * @return The list of living beings in the specified outside area.
     */
    public List<Alive> getAlivesByOutsideName(String name) {
        List<Alive> alives = new ArrayList<>();
        for (Outside outside : outsides.values()) {
            if (outside.getName().equals(name)) {
                alives.addAll(outside.getPeople());
                alives.addAll(outside.getAnimals());
            }
        }
        return alives;
    }
}
