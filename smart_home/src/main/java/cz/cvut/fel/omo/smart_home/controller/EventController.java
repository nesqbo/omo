package cz.cvut.fel.omo.smart_home.controller;

import cz.cvut.fel.omo.smart_home.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for handling events related to devices in the smart home system.
 */
@RestController
@RequestMapping("/event")
public class EventController {

    private final EventService eventService;

    /**
     * Constructor for the EventController class.
     *
     * @param eventService is for handling events.
     */
    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * Handles the POST request to break a device.
     * Breaks the specified device by creating a BreakDeviceEvent and triggering the event by visiting the device's interaction API.
     *
     * @param deviceName The name of the device to be broken.
     */
    @PostMapping("/break/{deviceName}")
    public void breakDevice(@PathVariable String deviceName) {
        eventService.breakDevice(deviceName);
    }
}
