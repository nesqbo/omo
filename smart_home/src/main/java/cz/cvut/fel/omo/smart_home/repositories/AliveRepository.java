package cz.cvut.fel.omo.smart_home.repositories;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

/**
 * The {@code AliveRepository} class manages the storage and retrieval of living beings in the smart home.
 */
@Getter
@Repository
@Slf4j
public class AliveRepository {

    private final HashMap<String, Alive> alives;

    public AliveRepository() {
        alives = new HashMap<>();
    }

    /**
     * Adds a living being to the repository.
     *
     * @param alive The living being to be added.
     * @throws IllegalArgumentException If a living being with the same name already exists.
     */
    public void addAlive(Alive alive) {

        if (alives.containsKey(alive.getName())) {
            log.error("Living being with name [[{}]] already exists", alive.getName());
            throw new IllegalArgumentException(
                    String.format("Living being with name %s already exists", alive.getName()));
        }
        alives.put(alive.getName(), alive);
    }

}
