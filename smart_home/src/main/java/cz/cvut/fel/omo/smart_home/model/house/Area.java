package cz.cvut.fel.omo.smart_home.model.house;

import cz.cvut.fel.omo.smart_home.model.alive.Animal;
import cz.cvut.fel.omo.smart_home.model.alive.Person;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Area} class is an abstract class representing an area within a smart home, such as a room or outdoor space.
 */
@Getter
@Setter
@Slf4j
public abstract class Area {
     private String name;
     private List<Person> people = new ArrayList<>();
     private List<Animal> animals = new ArrayList<>();
     private Floor floor;

     /**
      * Constructs an {@code Area} with the specified name on the given floor.
      *
      * @param name  The name of the area.
      * @param floor The floor to which the area belongs.
      */
     public Area(String name, Floor floor) {
          this.name = name;
          this.floor = floor;
          floor.addArea(this);
     }

     /**
      * Adds a person to the area.
      *
      * @param person The person to be added.
      */
     public void addPerson(Person person) {
          people.add(person);
     }

     /**
      * Adds an animal to the area.
      *
      * @param animal The animal to be added.
      */
     public void addAnimal(Animal animal) {
          animals.add(animal);
     }

     /**
      * Removes a person from the area.
      *
      * @param person The person to be removed.
      */
     public void removePerson(Person person) {
          if (!people.contains(person)) {
               log.error("Person [{}] is not in outside [{}]", person.getName(), name);
               return;
          }
          people.remove(person);
     }

     /**
      * Removes an animal from the area.
      *
      * @param animal The animal to be removed.
      */
     public void removeAnimal(Animal animal) {
       if (!animals.contains(animal)) {
            log.error("Animal [{}] is not in room [{}]", animal.getName(), name);
            return;
       }
       animals.remove(animal);
     }
}
