package cz.cvut.fel.omo.smart_home.simulation.chain;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.alive.Animal;
import cz.cvut.fel.omo.smart_home.model.alive.Person;
import cz.cvut.fel.omo.smart_home.model.alive.StrategyUtilClass;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.utils.HouseConfigUtil;
import cz.cvut.fel.omo.smart_home.model.factory.AliveFactory;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import cz.cvut.fel.omo.smart_home.simulation.services.NameGeneratorApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * The {@code AliveHandler} class is responsible for handling the creation of alive entities (persons and animals)
 * based on the configuration and adding them to rooms in the smart home simulation.
 */
@Component
public class AliveHandler extends BaseHandler {
    private final HouseConfigUtil houseConfigUtil;
    private final AliveRepository aliveRepository;
    private final AreaRepository areaRepository;
    private final NameGeneratorApiService nameGeneratorApiService;
    private final StrategyUtilClass strategyUtilClass;
    @Autowired
    public AliveHandler(OutsideHandler outsideHandler, AliveRepository aliveRepository, AreaRepository roomRepository, NameGeneratorApiService nameGeneratorApiService, HouseConfigUtil houseConfigUtil, StrategyUtilClass strategyUtilClass) {
        super(outsideHandler);
        this.aliveRepository = aliveRepository;
        this.areaRepository = roomRepository;
        this.nameGeneratorApiService = nameGeneratorApiService;
        this.houseConfigUtil = houseConfigUtil;
        this.strategyUtilClass = strategyUtilClass;
    }

    @Override
    public void handle() {
        int peopleToAdd;
        if (houseConfigUtil.personCount >= 3) {
            peopleToAdd = houseConfigUtil.personCount - 3;
            addPermanentResidents();
        } else {
            peopleToAdd = houseConfigUtil.personCount;
        }

        List<String> names = nameGeneratorApiService.getRandomNames(peopleToAdd + houseConfigUtil.animalCount);

        for (int i = 0; i < peopleToAdd; i++) {
            Person person = AliveFactory.createAlive(Alive.AliveType.PERSON, names.get(i), strategyUtilClass.getAllPersonStrategies());


            Room room = areaRepository.getRandomRoom();
            room.addPerson(person);
            person.setArea(room);
            person.setName(names.get(i));
            aliveRepository.addAlive(person);
        }

        for (int i = 0; i < houseConfigUtil.animalCount; i++) {
            Animal animal = AliveFactory.createAlive(Alive.AliveType.ANIMAL, names.get(peopleToAdd + i), strategyUtilClass.getAllAnimalStrategies());


            Room room = areaRepository.getRandomRoom();
            room.addAnimal(animal);
            animal.setArea(room);
            aliveRepository.addAlive(animal);
        }
        handleNext();
    }

    /**
     * Adds permanent residents (pre-defined persons) to a room in the smart home simulation.
     */
    private void addPermanentResidents() {
        Person tobias = AliveFactory.createAlive(Alive.AliveType.PERSON, "Tobias Le", List.of(strategyUtilClass.getRepairmanStrategy()));
        Person vanessa = AliveFactory.createAlive(Alive.AliveType.PERSON, "Vanessa Le", List.of(strategyUtilClass.getActiveStrategy()));
        Person jirka = AliveFactory.createAlive(Alive.AliveType.PERSON, "Jiri Sebek", List.of(strategyUtilClass.getJirkaStrategy()));

        Collection<Room> rooms = areaRepository.getRooms();
        Room room = rooms.stream().findFirst().orElseThrow();

        room.addPerson(tobias);
        room.addPerson(jirka);
        room.addPerson(vanessa);

        tobias.setArea(room);
        vanessa.setArea(room);
        jirka.setArea(room);

        aliveRepository.addAlive(tobias);
        aliveRepository.addAlive(vanessa);
        aliveRepository.addAlive(jirka);
    }
}
