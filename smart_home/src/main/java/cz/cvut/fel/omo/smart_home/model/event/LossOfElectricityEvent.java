package cz.cvut.fel.omo.smart_home.model.event;

import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import cz.cvut.fel.omo.smart_home.model.device.CircuitBreaker;
import cz.cvut.fel.omo.smart_home.model.deviceState.OffState;
import cz.cvut.fel.omo.smart_home.model.event.visitor.VisitedElement;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
@Slf4j
public class LossOfElectricityEvent extends Visitor {
    public LossOfElectricityEvent(DeviceRepository deviceRepository, AliveRepository aliveRepository) {
        super(deviceRepository, aliveRepository);
    }

    @Override
    public VisitedElement getRandomCompatibleElement() {
        return deviceRepository.getRandomDeviceOfType(DeviceType.CIRCUIT_BREAKER).getDeviceInteractionAPI();
    }

    @Override
    public void doForCircuitBreaker(CircuitBreaker circuitBreaker) {
        log.info("Loss of electricity event has been applied to circuit breaker [{}].", circuitBreaker.getDeviceInteractionAPI().getName());
        log.info("All non critical devices have been turned off.");
        deviceRepository
                .getAllDevices()
                .stream()
                .filter(device -> device.getDeviceInteractionAPI().getDeviceType() != DeviceType.CIRCUIT_BREAKER)
                .forEach(device -> device.getDeviceInteractionAPI().setState(new OffState()));
    }
}
