package cz.cvut.fel.omo.smart_home.simulation;

import cz.cvut.fel.omo.smart_home.simulation.chain.ChainOfResponsibility;
import cz.cvut.fel.omo.smart_home.simulation.chain.FloorHandler;
import cz.cvut.fel.omo.smart_home.utils.SimulationConfigUtil;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The {@code Simulation} class represents the main simulation component. It initializes and starts the simulation chain
 * of responsibility, sets up the simulation runnable, and starts the simulation thread.
 */
@Component
public class Simulation {
    private final FloorHandler floorHandler;
    private final SimulationRunnable simulationRunnable;
    private final SimulationConfigUtil simulationConfigUtil;

    @Autowired
    public Simulation(FloorHandler floorHandler, SimulationRunnable simulationRunnable, SimulationConfigUtil simulationConfigUtil) {
        this.floorHandler = floorHandler;
        this.simulationRunnable = simulationRunnable;
        this.simulationConfigUtil = simulationConfigUtil;
    }

    private void startChain() {
        new ChainOfResponsibility(floorHandler).startChain();
    }

    /**
     * Sets up the simulation by starting the simulation chain and initializing the simulation runnable.
     * Starts the simulation thread after setup.
     */
    @PostConstruct
    public void setUp() {
        startChain();

        simulationRunnable.setup(simulationConfigUtil.maxIterationsCount, simulationConfigUtil.iterationDelay);
        Thread thread = new Thread(simulationRunnable);
        thread.start();
    }
}
