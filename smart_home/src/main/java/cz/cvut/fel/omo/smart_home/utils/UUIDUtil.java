package cz.cvut.fel.omo.smart_home.utils;

/**
 * The {@code UUIDUtil} class provides utility methods for generating UUIDs (Universally Unique Identifiers).
 */
public class UUIDUtil {

    /**
     * Generates a random UUID and returns its first 5 characters.
     *
     * @return A string representing the first 5 characters of a randomly generated UUID.
     */
    public static String generateUUID() {
        return " "+java.util.UUID.randomUUID().toString().substring(0, 5);
    }
}
