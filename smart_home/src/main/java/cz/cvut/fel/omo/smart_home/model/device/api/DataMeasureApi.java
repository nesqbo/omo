package cz.cvut.fel.omo.smart_home.model.device.api;

import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;
import cz.cvut.fel.omo.smart_home.service.DataMeasureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Implementation of the DataMeasureAPI interface for measuring data from devices.
 */
@Slf4j
@Component
public class DataMeasureApi implements Subscriber {
    private final DataMeasureService dataMeasureService;

    @Autowired
    public DataMeasureApi(DataMeasureService dataMeasureService) {
        this.dataMeasureService = dataMeasureService;
    }


    @Override
    public void update(String deviceName, CollectibleData data) {
        dataMeasureService.createOrUpdateDeviceData(deviceName, data);
    }

    /**
     * Retrieves the collected data for a specific device.
     *
     * @param deviceName The name of the device for which data is retrieved.
     * @return The collected data for the specified device.
     */
    public CollectibleData getDataForDevice(String deviceName) {
        return dataMeasureService.getDataForDevice(deviceName);
    }

    /**
     * Retrieves all collected data for devices.
     *
     * @return A HashMap containing device names as keys and their corresponding collected data as values.
     */
    public HashMap<String, CollectibleData> getAllData() {
        return dataMeasureService.getAllData();
    }
}
