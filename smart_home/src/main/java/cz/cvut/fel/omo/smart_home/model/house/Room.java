package cz.cvut.fel.omo.smart_home.model.house;

import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import cz.cvut.fel.omo.smart_home.model.device.OutdoorBlinds;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Room} class represents a room within a smart home, containing windows and devices.
 */
@Slf4j
@Setter
@Getter
public class Room extends Area{
    private List<Window> windows = new ArrayList<>();
    private List<Device> devices = new ArrayList<>();

    public Room(Floor floor, String name) {
        super(name, floor);
    }

    /**
     * Adds a window to the room.
     *
     * @param window The window to be added.
     */
    public void addWindow(Window window) {
        windows.add(window);
    }

    /**
     * Adds a device to the room. If the device is of type {@code OutdoorBlinds}, it creates a corresponding window.
     *
     * @param device The device to be added.
     */
    public void addDevice(Device device) {
        if (device.getDeviceInteractionAPI().getDeviceType() == DeviceType.OUTDOOR_BLINDS) {
            Window window = new Window();
            OutdoorBlinds outdoorBlinds = (OutdoorBlinds) device;
            window.setOutdoorBlinds(outdoorBlinds);
            addWindow(window);
        }
        log.info("Adding device [{}] to room [{}]", device.getDeviceInteractionAPI().getName(), getName());
        devices.add(device);
    }

    /**
     * Removes a device from the room based on the device name.
     *
     * @param deviceName The name of the device to be removed.
     */
    public void removeDeviceByDeviceName(String deviceName) {
        devices.removeIf(device -> device.getDeviceInteractionAPI().getName().equals(deviceName));
    }
}
