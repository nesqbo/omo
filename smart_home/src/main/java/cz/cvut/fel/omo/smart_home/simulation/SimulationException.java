package cz.cvut.fel.omo.smart_home.simulation;

/**
 * The {@code SimulationException} class represents an exception specific to the simulation module.
 * It extends the {@link RuntimeException} class and is used to indicate exceptional conditions during simulation execution.
 */
public class SimulationException extends RuntimeException {

    /**
     * Constructs a new {@code SimulationException} with the specified detail message.
     *
     * @param message the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     */
    public SimulationException(String message) {
        super(message);
    }
}
