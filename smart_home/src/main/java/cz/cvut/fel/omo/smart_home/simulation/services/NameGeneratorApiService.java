package cz.cvut.fel.omo.smart_home.simulation.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.omo.smart_home.NameGeneratorApiConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code ApiService} class provides methods to interact with an external API for generating random names.
 */
@Service
@Slf4j
public class NameGeneratorApiService {
    private final NameGeneratorApiConfig nameGeneratorApiConfig;

    @Autowired
    public NameGeneratorApiService(NameGeneratorApiConfig nameGeneratorApiConfig) {
        this.nameGeneratorApiConfig = nameGeneratorApiConfig;
    }

    /**
     * Retrieves a specified number of random names from the external API.
     *
     * @param count The number of random names to retrieve.
     * @return A list of randomly generated names.
     */
    public List<String> getRandomNames(int count) {
        if (count < 0) {
            throw new IllegalArgumentException("Invalid count: " + count);
        } else if (count == 0) {
            return new ArrayList<>();
        }
        List<String> names = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        int namesLeftToGenerate = count;
        int namesToGenerateThisIteration;

        while (namesLeftToGenerate > 0) {
            namesToGenerateThisIteration = Math.min(namesLeftToGenerate, 25);
            namesLeftToGenerate -= namesToGenerateThisIteration;

            String url = generateUrl(namesToGenerateThisIteration);
            String response = restTemplate.getForObject(url, String.class);
            names.addAll(parseNameFromJson(response));
        }

        return names;
    }

    private List<String> parseNameFromJson(String jsonString) {
        try {
            List<String> names = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(jsonString);

            JsonNode dataArray = jsonNode.path("data");

            for (JsonNode data : dataArray) {
                String firstName = data
                        .path("name")
                        .path("firstname")
                        .path("name")
                        .asText();

                String lastName = data
                        .path("name")
                        .path("lastname")
                        .path("name")
                        .asText();


                StringBuilder sb = new StringBuilder();
                names.add(sb.append(firstName).append(" ").append(lastName).toString());
            }
            return names;
        } catch (Exception e) {
            log.error("Error parsing name from json: " + e.getMessage());
            throw new RuntimeException("Error parsing name from json: " + e.getMessage());
        }
    }

    private String generateUrl(int count) {
        //example url https://api.parser.name/?api_key=20410f20faeb6911d171d4b634452112&endpoint=generate&country_code=CZ

        return nameGeneratorApiConfig.getHostname() +
                "?api_key=" +
                nameGeneratorApiConfig.getKey() +
                "&endpoint=generate&country_code=CZ" +
                "&results=" +
                count;
    }
}