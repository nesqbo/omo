package cz.cvut.fel.omo.smart_home.model.alive.personStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.house.Outside;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
@Scope("prototype")
public class ActiveStrategy extends BaseStrategy{
    @Override
    public void act(Alive actor) {

        int randomInt = new Random().nextInt(10);

        if (randomInt < 7){
            Outside outside = findNextOutside();
            log.info("ActivePersonStrategy [{}]: I am moving to another place [{}]", actor.getName(), outside.getName());
            actor.moveToArea(outside);
        } else {
            Room room = findNextRoom();
            log.info("ActivePersonStrategy [{}]: I am moving to another area [{}]", actor.getName(), room.getName());
            actor.moveToArea(room);
        }
    }

    @Override
    public void beInteractedWith() {
        log.info("ActivePersonStrategy : I am being interacted with, I am active so I try to be outside as much as possible.");

    }

    @Override
    public void interactWith(Alive actor, InteractibleEntity target) {
        if (getCurrentArea(actor) instanceof Room) {
            log.info("ActivePersonStrategy [{}]: I am interacting with [{}] in room [{}] but I don't want to stay here so I will move to outside [{}]", actor.getName(), target.getName(), getCurrentArea(actor).getName(), findNextOutside().getName());
        }
    }

    private Outside findNextOutside(){
        return areaRepository.getRandomOutside();
    }

    private Room findNextRoom(){
        return areaRepository.getRandomRoom();
    }
}
