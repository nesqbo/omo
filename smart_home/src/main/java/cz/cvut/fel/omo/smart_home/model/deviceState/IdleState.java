package cz.cvut.fel.omo.smart_home.model.deviceState;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IdleState extends DeviceState {

    public IdleState() {
        super(5, 0, 0, 5);
        stateName = "Idle";
    }

    public IdleState(int electricityConsumption, int gasConsumption, int waterConsumption, int conditionDecrease) {
        super(electricityConsumption, gasConsumption, waterConsumption, conditionDecrease);
        stateName = "Idle";
    }

    @Override
    public void changeToNextState(DeviceContext context) {
        DeviceState newState = context.getDeviceStates().get("Off");
        context.setState(newState);
    }

}
