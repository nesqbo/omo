package cz.cvut.fel.omo.smart_home.model.alive;

import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.BaseStrategy;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;

import cz.cvut.fel.omo.smart_home.model.house.Area;
import cz.cvut.fel.omo.smart_home.model.house.Outside;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Class representing an animal in the smart home system.
 */
@AllArgsConstructor
@Slf4j
@Getter
@Setter
public class Animal implements Alive {
    private String name;
    private Area area;
    private BaseStrategy strategy;


    public Animal(String name, BaseStrategy strategy) {
        this.name = name;
        this.strategy = strategy;
    }

    @Override
    public void moveToRoom(Room area) {
        log.info("Animal [{}] is moving from area [{}] to area [{}]", name, this.area.getName(), area.getName());
        this.area.removeAnimal(this);
        this.area = area;
        area.addAnimal(this);

    }

    @Override
    public void moveToArea(Area area) {
        log.info("Animal [{}] is moving from area [{}] to area [{}]", name, this.area.getName(), area.getName());
        this.area.removeAnimal(this);
        this.area = null;
        area.addAnimal(this);
    }

    @Override
    public void interactWith(InteractibleEntity interactibleEntity) {
        log.info("Person [{}] is interacting with [{}]", name, interactibleEntity.getName());
        strategy.interactWith(this, interactibleEntity);

    }

    @Override
    public void moveToOutside(Outside outside) {
        log.info("Person [{}] is moving from area [{}] to outside [{}]", name, this.area.getName(), outside.getName());
        this.area.removeAnimal(this);
        this.area = null;
        outside.addAnimal(this);
    }

    @Override
    public void beInteractedWith() {
        strategy.beInteractedWith();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void act() {
        log.info("\n");
        strategy.act(this);
    }
}
