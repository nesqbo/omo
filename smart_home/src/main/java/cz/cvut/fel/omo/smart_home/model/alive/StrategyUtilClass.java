package cz.cvut.fel.omo.smart_home.model.alive;

import cz.cvut.fel.omo.smart_home.model.alive.animalStrategy.CatStrategy;
import cz.cvut.fel.omo.smart_home.model.alive.animalStrategy.DogStrategy;
import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.IntrovertedStrategy;
import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.JirkaStrategy;
import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.RepairmanStrategy;
import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.TalkativeStrategy;
import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.ActiveStrategy;
import cz.cvut.fel.omo.smart_home.model.alive.personStrategy.BaseStrategy;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Utility class for accessing and managing different strategies for alive entities.
 */
@Component
@Getter
public class StrategyUtilClass {
    private final TalkativeStrategy talkativeStrategy;
    private final JirkaStrategy jirkaStrategy;
    private final IntrovertedStrategy introvertedStrategy;
    private final RepairmanStrategy repairmanStrategy;
    private final ActiveStrategy activeStrategy;
    private final DogStrategy dogStrategy;
    private final CatStrategy catStrategy;

    @Autowired
    public StrategyUtilClass(TalkativeStrategy talkativeStrategy, JirkaStrategy jirkaStrategy,
                             IntrovertedStrategy introvertedStrategy, RepairmanStrategy repairmanStrategy, ActiveStrategy activeStrategy,
                             DogStrategy dogStrategy, CatStrategy catStrategy) {
        this.talkativeStrategy = talkativeStrategy;
        this.jirkaStrategy = jirkaStrategy;
        this.introvertedStrategy = introvertedStrategy;
        this.repairmanStrategy = repairmanStrategy;
        this.activeStrategy = activeStrategy;
        this.dogStrategy = dogStrategy;
        this.catStrategy = catStrategy;
    }

    /**
     * Retrieves a list of all animal strategies.
     *
     * @return List of all animal strategies.
     */
    public List<BaseStrategy> getAllAnimalStrategies() {
        return List.of(
                dogStrategy,
                catStrategy
        );
    }

    /**
     * Retrieves a list of all person strategies.
     *
     * @return List of all person strategies.
     */
    public List<BaseStrategy> getAllPersonStrategies() {
        return List.of(
                talkativeStrategy,
                jirkaStrategy,
                introvertedStrategy,
                repairmanStrategy,
                activeStrategy
        );
    }
}
