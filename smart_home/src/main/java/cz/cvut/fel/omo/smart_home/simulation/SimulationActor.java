package cz.cvut.fel.omo.smart_home.simulation;

/**
 * An interface representing an actor in the simulation that can perform actions.
 */
public interface SimulationActor {

    /**
     * Defines the behavior of the actor when it performs an action in the simulation.
     */
    void act();
}
