package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Publisher;
import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;
import cz.cvut.fel.omo.smart_home.model.deviceState.DeviceState;
import cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState;
import cz.cvut.fel.omo.smart_home.model.deviceState.BrokenState;
import cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext;
import cz.cvut.fel.omo.smart_home.model.deviceState.IdleState;
import cz.cvut.fel.omo.smart_home.model.deviceState.OffState;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.device.strategy.ManualRepairStrategy;
import cz.cvut.fel.omo.smart_home.model.device.strategy.RepairStrategy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.VisitedElement;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import cz.cvut.fel.omo.smart_home.simulation.SimulationActor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;


/**
 * Interface for all devices.
 * Implements a Observer design pattern
 */
@Slf4j
public abstract class Device {
    protected String deviceName;
    protected List<Subscriber> subscribedApis;
    private final CollectibleData collectibleData;
    protected DeviceState state = new ActiveState(); // normally would be off but the results are not interesting
    private final DeviceType deviceType;

    private RepairStrategy repairStrategy = new ManualRepairStrategy();
    private final RepairProxy repairProxy;
    private final HashMap<String, DeviceState> deviceStates = new HashMap<>();

    {
        deviceStates.put("Active", new ActiveState());
        deviceStates.put("Idle", new IdleState());
        deviceStates.put("Off", new OffState());
        deviceStates.put("Broken", new BrokenState());
    }

    @Getter
    private transient final DeviceInteractionAPI deviceInteractionAPI = new DeviceInteractionAPI();


    public Device(String name, DeviceType deviceType, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        this.deviceName = name;
        collectibleData = new CollectibleData();
        this.deviceType = deviceType;
        this.subscribedApis = subscribedApis;
        this.repairProxy = repairProxy;
        notifyApis(collectibleData);
    }

    private void addSubscriber(Subscriber subscriber) {
        subscribedApis.add(subscriber);
    }

    private void removeSubscriber(Subscriber subscriber) {
        subscribedApis.remove(subscriber);
    }

    /**
     * Method for notifying all APIs.
     * see {@link Subscriber}
     */
    private void notifyApis(CollectibleData data) {
        for (Subscriber api : subscribedApis) {
            api.update(deviceName, data);
        }
    }

    private void beInteractedWith() {
        if (this.isActive()) {
            log.info("Device [{}] is already active.", deviceName);
        } else {
            log.info("Device [{}] is being interacted with.", deviceName);
            state.changeToNextState(this.deviceInteractionAPI);
        }
    }

    /**
     * Initiates the actuation of the device. This includes updating consumable data,
     * decreasing condition, and transitioning to the next state.
     */
    protected void act() {
        increaseElectricityConsumption(state.getElectricityConsumption());
        increaseGasConsumption(state.getGasConsumption());
        increaseWaterConsumption(state.getWaterConsumption());
        decreaseCondition(state.getConditionDecrease());
        this.state.changeToNextState(this.deviceInteractionAPI);
    }

    private String getName() {
        return deviceName;
    }

    /**
     * Increases the electricity consumption of the device.
     *
     * @param amount The amount by which electricity consumption should be increased.
     */
    protected void increaseElectricityConsumption(double amount) {
        if (amount > 0) {
            log.info("Increasing electricity consumption of device [{}] by [{}]", deviceName, amount);
            collectibleData.addElectricity(amount);
            notifyApis(collectibleData);
        }
    }

    private List<Subscriber> getSubscribedApis() {
        return subscribedApis;
    }

    private CollectibleData getCollectibleData() {
        return collectibleData;
    }

    private DeviceState getState() {
        return state;
    }

    private DeviceType getDeviceType() {
        return deviceType;
    }

    private HashMap<String, DeviceState> getDeviceStates() {
        return deviceStates;
    }

    /**
     * Increases the water consumption of the device.
     *
     * @param amount The amount by which water consumption should be increased.
     */
    protected void increaseWaterConsumption(double amount) {
        if (amount > 0) {
            log.info("Increasing water consumption of device [{}] by [{}]", deviceName, amount);
            collectibleData.addWater(amount);
            notifyApis(collectibleData);
        }
    }

    /**
     * Increases the gas consumption of the device.
     *
     * @param amount The amount by which gas consumption should be increased.
     */
    protected void increaseGasConsumption(double amount) {
        if (amount > 0) {
            log.info("Increasing gas consumption of device [{}] by [{}]", deviceName, amount);
            collectibleData.addGas(amount);
            notifyApis(collectibleData);
        }
    }

    /**
     * Decreases the condition of the device.
     *
     * @param amount The amount by which the condition should be decreased.
     */
    protected void decreaseCondition(double amount) {
        if (amount > 0) {
            log.info("Decreasing condition of device [{}] by [{}]", deviceName, amount);
            collectibleData.decreaseCondition(amount, this.deviceInteractionAPI);
            notifyApis(collectibleData);
        }

    }

    private void repair() {
        log.info("Reading manual for device [{}]", deviceName);
        repairStrategy.repair(this.deviceInteractionAPI);
    }

    private void replace() {
        log.info("Replacing device [{}]", deviceName);
        repairProxy.replace(this.deviceInteractionAPI);
    }

    public boolean isBroken() {
        return collectibleData.getCondition() <= 0;
    }

    public boolean isActive() {
        return state instanceof ActiveState;
    }

    /**
     * Accepts a Visitor, allowing it to perform operations on the device.
     *
     * @param visitor The Visitor to be accepted.
     */
    protected abstract void accept(Visitor visitor);

    private void setState(DeviceState state) {
        String stateName = state.getStateName();
        this.state = deviceStates.get(stateName);
    }

    protected void setActiveState(ActiveState state) {
        this.deviceStates.put("Active", state);
        setState(state);
    }

    protected void setIdleState(IdleState state) {
        this.deviceStates.put("Idle", state);
    }


    /**
     * Sets the repair strategy for the device.
     *
     * @param repairStrategy The repair strategy to be set.
     */
    protected void setRepairStrategy(RepairStrategy repairStrategy) {
        this.repairStrategy = repairStrategy;
    }

    /**
     * Inner class representing the interaction API of the device.
     */
    public class DeviceInteractionAPI implements DeviceContext, InteractibleEntity, SimulationActor, VisitedElement, Publisher {

        /**
         * Initiates the repair process for the device.
         */
        public void repairDevice() {
            repair();
        }

        /**
         * Accepts a Visitor, allowing it to perform operations on the device.
         *
         * @param visitor The Visitor to be accepted.
         */
        public void accept(Visitor visitor) {
            if (Device.this.isBroken()) {
                log.warn("Device [{}] is broken and cannot be interacted with.", deviceName);
                return;
            }
            Device.this.accept(visitor);
        }

        /**
         * Decreases the condition of the device by a specified amount.
         *
         * @param randomInt The amount by which the condition should be decreased.
         */
        public void decreaseCondition(int randomInt) {
            Device.this.decreaseCondition(randomInt);
        }

        @Override
        public void setState(DeviceState state) {
            Device.this.setState(state);
        }

        @Override
        public void beInteractedWith() {
            if (Device.this.isBroken()) {
                log.warn("Device [{}] is broken and cannot be interacted with.", deviceName);
                return;
            }
            Device.this.beInteractedWith();
        }

        @Override
        public String getName() {
            return Device.this.getName();
        }

        @Override
        public DeviceType getDeviceType() {
            return Device.this.getDeviceType();
        }

        @Override
        public CollectibleData getCollectibleData() {
            return Device.this.getCollectibleData();
        }

        @Override
        public List<Subscriber> getSubscribedApis() {
            return Device.this.getSubscribedApis();
        }

        @Override
        public DeviceState getState() {
            return Device.this.getState();
        }

        @Override
        public void replace() {
            Device.this.replace();
        }

        @Override
        public HashMap<String, DeviceState> getDeviceStates() {
            return Device.this.getDeviceStates();
        }

        @Override
        public DeviceInteractionAPI getDeviceInteractionAPI() {
            return this;
        }

        @Override
        public void act() {
            if (Device.this.isBroken()) {
                return;
            }
            Device.this.act();
        }

        @Override
        public void notifySubscribers(CollectibleData data) {
            Device.this.notifyApis(data);
        }

        @Override
        public void addSubscriber(Subscriber subscriber) {
            Device.this.addSubscriber(subscriber);
        }

        @Override
        public void removeSubscriber(Subscriber subscriber) {
            Device.this.removeSubscriber(subscriber);
        }
    }
}
