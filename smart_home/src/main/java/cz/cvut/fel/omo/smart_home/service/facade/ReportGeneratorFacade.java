package cz.cvut.fel.omo.smart_home.service.facade;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.api.DataMeasureApi;
import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;
import cz.cvut.fel.omo.smart_home.model.house.Outside;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import cz.cvut.fel.omo.smart_home.utils.HouseConfigUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * The {@code ReportGeneratorFacade} class provides methods for generating and writing consumption reports.
 */
@Component
@Slf4j
public class ReportGeneratorFacade {

    @Autowired
    DataMeasureApi dataMeasureApi;
    private final LocalDateTime time;

    private final HouseConfigUtil houseConfigUtil;
    private final AreaRepository areaRepository;

    /**
     * Constructs a {@code ReportGeneratorFacade} with the specified {@code DataMeasureApiImpl}.
     *
     * @param dataMeasureApi  The data measure API implementation.
     * @param houseConfigUtil
     * @param areaRepository
     */
    public ReportGeneratorFacade(DataMeasureApi dataMeasureApi, HouseConfigUtil houseConfigUtil, AreaRepository areaRepository) {
        this.dataMeasureApi = dataMeasureApi;
        this.houseConfigUtil = houseConfigUtil;
        this.areaRepository = areaRepository;
        this.time = LocalDateTime.now();
    }

    private HashMap<String, CollectibleData> getCurrentData() {
        return dataMeasureApi.getAllData();
    }

    /**
     * Generates a consumption report for all devices.
     *
     * @return A JSON object representing the consumption report.
     */
    public JSONObject generateConsumptionReport() {
        JSONObject result = new JSONObject();
        HashMap<String, CollectibleData> devices = getCurrentData();


        log.info("Generating report for all devices...");
        devices.forEach((k, v) -> result.put(k,new JSONObject(v).put("time", time)));
        return result;
    }

    /**
     * Generates a consumption report for a specific device.
     *
     * @param deviceName The name of the device for which the report is generated.
     * @return A JSON object representing the device consumption report.
     */
    public JSONObject generateDeviceReport(String deviceName) {

        JSONObject result = new JSONObject();
        CollectibleData data = dataMeasureApi.getDataForDevice(deviceName);


        log.info("Generating report for device [{}]...", deviceName);
        result.put(deviceName, new JSONObject(data).put("time", time));
        return result;
    }

    /**
     * Writes a generated report to a file based on the specified report type.
     *
     * @param report     The JSON object representing the report.
     * @param reportType The type of the report (CONSUMPTION, ALIVES, HOUSE).
     */
    public void writeToFile(JSONObject report, ReportType reportType) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String formattedDateTime = now.format(formatter);
        String fileName = "";

        if (reportType == ReportType.CONSUMPTION) {
            fileName = "src/main/logs/consumption/consumption_report_" + formattedDateTime + ".json";
        } else if (reportType == ReportType.ALIVES) {
            fileName = "src/main/logs/alive/alive_report_" + formattedDateTime + ".json";
        } else if (reportType == ReportType.HOUSE) {
            fileName = "src/main/logs/house/house_report_" + formattedDateTime + ".json";
        }

        log.info("Writing report to file [{}]...", fileName);
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(report.toString(2));
            fileWriter.close();
            log.info("Report successfully written to file [{}]...", fileName);
        } catch (IOException e){
            log.error("Error {} while writing report to file [{}]...", e.getMessage(), fileName);
        }
    }

    /**
     * Enum representing the types of reports that can be generated.
     */
    public enum ReportType {
        CONSUMPTION,
        ALIVES,
        HOUSE
    }


    /**
     * Generates a report for the entire house, including information about floors, rooms, and outsides.
     *
     * @return A JSON object representing the house report.
     */
    public JSONObject generateHouseReport(){

        JSONObject result = new JSONObject();
        JSONArray floorsArray = new JSONArray();
        floorsArray.put(houseConfigUtil.floorCount);

        JSONObject areasObject = new JSONObject();
        JSONObject roomsObject = new JSONObject();
        JSONArray outsidesArray = new JSONArray();

        for (Room room : areaRepository.getRooms()) {
            JSONObject roomArray = processRoom(room);
            roomsObject.put(room.getName(), roomArray);
        }

        for (Outside outside : areaRepository.getOutsides()) {
            JSONObject outsideArray = processOutside(outside);
            outsidesArray.put(outsideArray);
        }

        areasObject.put("rooms", roomsObject);
        areasObject.put("outsides", outsidesArray);
        result.put("areas", areasObject);
        result.put("floors", floorsArray);

        return result;

    }

    /**
     * Generates a report for all living beings in the house, including those in rooms and outsides.
     *
     * @return A JSON object representing the alive report.
     */
    public JSONObject generateAliveReport(){
        JSONObject result = new JSONObject();

        JSONObject roomsObject = new JSONObject();

        Collection<Room> rooms = areaRepository.getRooms();
        Collection<Outside> outsides = areaRepository.getOutsides();

        for (Room room : rooms) {
            List<Alive> alives = areaRepository.getAlivesByRoomName(room.getName());

            JSONArray roomArray = new JSONArray();
            for (Alive alive : alives) {
                roomArray.put(alive.getName());
            }

            if (!roomArray.isEmpty()) {
                roomsObject.put(room.getName(), roomArray);
            }
        }

        for (Outside outside : outsides) {
            List<Alive> alives = areaRepository.getAlivesByOutsideName(outside.getName());

            JSONArray roomArray = new JSONArray();
            for (Alive alive : alives) {
                roomArray.put(alive.getName());
            }

            if (!roomArray.isEmpty()) {
                roomsObject.put(outside.getName(), roomArray);
            }
        }

        result.put("alives", roomsObject);
        return result;
    }

    private JSONObject processRoom(Room room) {
        JSONObject roomObject = new JSONObject();
        JSONArray devicesArray = new JSONArray();
        JSONArray alivesArray = new JSONArray();

        for (Device device : areaRepository.getDevicesByRoomName(room.getName())) {
            devicesArray.put(device.getDeviceInteractionAPI().getName());
        }

        if (!room.getAnimals().isEmpty() || !room.getPeople().isEmpty()) {
            for (Alive alive : room.getPeople()) {
                alivesArray.put(alive.getName());
            }

            roomObject.put("devices", devicesArray);
            roomObject.put("alives", alivesArray);
        } else {
            roomObject.put("devices", devicesArray);
            roomObject.put("alives", new JSONArray());
        }

        return roomObject;
    }


    private JSONObject processOutside(Outside outside) {
        JSONObject outsideObject = new JSONObject();
        List<String> aliveNames = new ArrayList<>();

        if (!outside.getAnimals().isEmpty() || !outside.getPeople().isEmpty()) {
            aliveNames.addAll(outside.getPeople().stream().map(Alive::getName).toList());
            aliveNames.addAll(outside.getAnimals().stream().map(Alive::getName).toList());
        }

        outsideObject.put(outside.getName(), aliveNames);
        return outsideObject;
    }


}
