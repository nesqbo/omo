package cz.cvut.fel.omo.smart_home.model.alive.personStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@Scope("prototype")
public class RepairmanStrategy extends BaseStrategy {
    @Override
    public void act(Alive actor) {
        Room currentRoom = (Room) getCurrentArea(actor);

        if (roomContainsBrokenDevice(currentRoom)) {
            log.info("RepairmanStrategyPerson [{}]: I found a broken device. I will repair it.", actor.getName());
            repairFirstBrokenDeviceInRoom(currentRoom);
        } else {
            areaRepository.findRoomWithBrokenDevice()
                    .ifPresentOrElse(roomToMoveTo -> {
                        log.info("RepairmanStrategyPerson [{}]: I found a broken device in room [{}]. I will go there.", actor.getName(), roomToMoveTo.getName());
                        actor.moveToRoom(roomToMoveTo);
                    }, () -> {
                        log.info("RepairmanStrategyPerson [{}]: I didn't find any broken devices. I will rest.", actor.getName());
                    });
        }
    }

    @Override
    public void beInteractedWith() {
        log.info("RepairmanStrategyPerson: Don't talk to me. I'm looking for broken devices.");
    }

    private boolean roomContainsBrokenDevice(Room room) {
        for (Device device : room.getDevices()) {
            if (device.isBroken()) {
                return true;
            }
        }
        return false;
    }

    private void repairFirstBrokenDeviceInRoom(Room room) {
        for (Device device : room.getDevices()) {
            if (device.isBroken()) {
                device.getDeviceInteractionAPI().repairDevice();
                return;
            }
        }
    }
}
