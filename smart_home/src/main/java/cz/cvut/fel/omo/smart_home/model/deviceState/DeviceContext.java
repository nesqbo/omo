package cz.cvut.fel.omo.smart_home.model.deviceState;

import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;
import cz.cvut.fel.omo.smart_home.model.device.DeviceType;

import java.util.HashMap;
import java.util.List;

/**
 * Interface representing the context of a device in the smart home system.
 */
public interface DeviceContext {

    /**
     * Sets the state of the device.
     *
     * @param state The new state of the device.
     */
    void setState(DeviceState state);

    /**
     * Retrieves the name of the device.
     *
     * @return The name of the device.
     */
    String getName();

    /**
     * Retrieves the type of the device.
     *
     * @return The type of the device.
     */
    DeviceType getDeviceType();

    /**
     * Retrieves the collectible data of the device.
     *
     * @return The collectible data of the device.
     */
    CollectibleData getCollectibleData();

    /**
     * Retrieves the subscribed APIs of the device.
     *
     * @return The subscribed APIs of the device.
     */
    List<Subscriber> getSubscribedApis();

    /**
     * Retrieves the current state of the device.
     *
     * @return The current state of the device.
     */
    DeviceState getState();

    /**
     * Initiates the replacement of the device.
     */
    void replace();

    /**
     * Retrieves a mapping of device states.
     *
     * @return A mapping of device states.
     */
    HashMap<String, DeviceState> getDeviceStates();

    /**
     * Retrieves the interaction API of the device.
     *
     * @return The interaction API of the device.
     */
    Device.DeviceInteractionAPI getDeviceInteractionAPI();
}
