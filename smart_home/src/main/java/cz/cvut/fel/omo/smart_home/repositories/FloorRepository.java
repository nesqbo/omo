package cz.cvut.fel.omo.smart_home.repositories;

import cz.cvut.fel.omo.smart_home.model.house.Floor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;

/**
 * The {@code FloorRepository} class manages the storage and retrieval of floors in the smart home.
 */
@Repository
@Slf4j
public class FloorRepository {

    private final HashMap<Integer, Floor> floors;
    public FloorRepository() {
        floors = new HashMap<>();
    }

    /**
     * Adds a floor to the repository.
     *
     * @param floor The floor to be added.
     * @throws IllegalArgumentException If a floor with the same number already exists.
     */
    public void addFloor(Floor floor) {
        if (floors.containsKey(floor.getFloorNumber())) {
            log.error("Floor with number [[{}]] already exists", floor.getFloorNumber());
            throw new IllegalArgumentException(
                    String.format("Floor with number %d already exists", floor.getFloorNumber()));
        }
        floors.put(floor.getFloorNumber(), floor);
    }

    /**
     * Retrieves a floor from the repository based on the floor number.
     *
     * @param floorNumber The number of the floor to be retrieved.
     * @return The floor with the specified number.
     * @throws IllegalArgumentException If no floor with the specified number exists.
     */
    public Floor getFloor(int floorNumber){
        if (!floors.containsKey(floorNumber)) {
            log.error("Floor with number [[{}]] does not exist", floorNumber);
            throw new IllegalArgumentException(
                    String.format("Floor with number %d does not exist", floorNumber));
        }
        return floors.get(floorNumber);
    }

    /**
     * Returns a collection of all floors in the repository.
     *
     * @return The collection of floors.
     */
    public Collection<Floor> getAllFloors() {
        return floors.values();
    }

}
