package cz.cvut.fel.omo.smart_home.simulation;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * The {@code SimulationIterator} class is responsible for iterating over a list of {@link SimulationActor}s during simulation execution.
 * It provides methods to check for the existence of the next actor, retrieve the next actor, reset the iterator, and get the current actor.
 */
@Slf4j
public class SimulationIterator {
    private final List<SimulationActor> simulationActors;
    private int index;

    /**
     * Constructs a new {@code SimulationIterator} with the specified list of simulation actors.
     *
     * @param simulationActors the list of simulation actors to iterate over.
     * @throws SimulationException if the list of simulation actors is empty.
     */
    public SimulationIterator(List<SimulationActor> simulationActors) {
        this.simulationActors = simulationActors;
        if (simulationActors.isEmpty()) {
            throw new SimulationException("No simulation actors. Failure to create iterator.");
        }
        index = 0;
    }

    /**
     * Checks if there is a next simulation actor in the iterator.
     *
     * @return {@code true} if there is a next simulation actor, {@code false} otherwise.
     */
    public boolean hasNext() {
        return index < simulationActors.size() - 1;
    }

    /**
     * Retrieves the next simulation actor in the iterator.
     *
     * @return the next simulation actor.
     * @throws SimulationException if there is no next element in the iterator.
     */
    public SimulationActor next() {
        if (hasNext()) {
            return simulationActors.get(++index);
        } else {
            throw new SimulationException("No next element in simulation iterator.");
        }
    }

    /**
     * Resets the iterator to the beginning.
     *
     * @throws SimulationException if the reset is attempted before the end of iteration.
     */
    public void reset() {
        if (index < simulationActors.size() - 1) {
            throw new SimulationException("Reset before end of iteration, maybe running too fast. Adjust the speed");
        }
        index = 0;
    }

    /**
     * Gets the current simulation actor in the iterator.
     *
     * @return the current simulation actor.
     */
    public SimulationActor current() {
        return simulationActors.get(index);
    }
}
