package cz.cvut.fel.omo.smart_home.model.device.strategy;

import cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

@Slf4j
public class ManualRepairStrategy implements RepairStrategy {
    public ManualRepairStrategy() {
    }

    @Override
    public void repair(DeviceContext context) {
        log.info("Device [{}] can be manually repaired.", context.getName());
        Random random = new Random();
        int restoreBy = random.nextInt(1, 10)*10;
        context.getCollectibleData().restoreCondition(restoreBy, context);
    }
}
