package cz.cvut.fel.omo.smart_home.model.event;

import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import cz.cvut.fel.omo.smart_home.model.device.OutdoorBlinds;
import cz.cvut.fel.omo.smart_home.model.device.WindSensor;
import cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState;
import cz.cvut.fel.omo.smart_home.model.event.visitor.VisitedElement;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
@Slf4j
public class StrongWindEvent extends Visitor {
    @Autowired
    public StrongWindEvent(DeviceRepository deviceRepository, AliveRepository aliveRepository) {
        super(deviceRepository, aliveRepository);
    }

    @Override
    public VisitedElement getRandomCompatibleElement() {
        return deviceRepository.getRandomDeviceOfType(DeviceType.WIND_SENSOR).getDeviceInteractionAPI();
    }

    @Override
    public void doForWindSensor(WindSensor windSensor) {
        log.info("Strong wind event has been applied to wind sensor [{}].", windSensor.getDeviceInteractionAPI().getName());
        for (OutdoorBlinds outdoorBlind : windSensor.getOutdoorBlinds()) {
            log.info("Outdoor blind [{}] has been closed due to strong wind.", outdoorBlind.getDeviceInteractionAPI().getName());
            outdoorBlind.getDeviceInteractionAPI().setState(new ActiveState());
        }
    }
}
