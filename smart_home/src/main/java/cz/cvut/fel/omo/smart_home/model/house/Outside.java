package cz.cvut.fel.omo.smart_home.model.house;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
public class Outside extends Area {
    public Outside(Floor floor, String name) {
        super(name, floor);
    }
}
