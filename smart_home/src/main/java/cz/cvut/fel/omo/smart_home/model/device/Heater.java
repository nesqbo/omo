package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Represents a heater device in the smart home system.
 */
@Slf4j
public class Heater extends Device {

    /**
     * Creates a new instance of the Heater device with the specified parameters.
     *
     * @param name           The name of the heater.
     * @param subscribedApis The list of data measure APIs to which the heater subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public Heater(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.HEATER, subscribedApis, repairProxy);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForHeater(this);
    }
}
