package cz.cvut.fel.omo.smart_home.simulation.chain;

/**
 * The {@code BaseHandler} class provides a base implementation for the {@code Handler} interface.
 * It includes methods for setting the next handler in the chain and handling the next request in the chain.
 */
public abstract class BaseHandler implements Handler {
    private Handler next;
    public BaseHandler(Handler next) {
        this.next = next;
    }

    @Override
    public void setNext(Handler handler) {
        this.next = handler;
    }

    @Override
    public abstract void handle();

    /**
     * Handles the next request in the chain. If the next handler is not set, no action is taken.
     */
    public void handleNext () {
        if (next != null) {
            next.handle();
        }
    }
}
