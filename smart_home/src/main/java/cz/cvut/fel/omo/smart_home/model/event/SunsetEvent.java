package cz.cvut.fel.omo.smart_home.model.event;

import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import cz.cvut.fel.omo.smart_home.model.device.Light;
import cz.cvut.fel.omo.smart_home.model.device.LightSensor;
import cz.cvut.fel.omo.smart_home.model.deviceState.IdleState;
import cz.cvut.fel.omo.smart_home.model.event.visitor.VisitedElement;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Slf4j
@Configurable
public class SunsetEvent extends Visitor {
    @Autowired
    public SunsetEvent(DeviceRepository deviceRepository, AliveRepository aliveRepository) {
        super(deviceRepository, aliveRepository);
    }

    @Override
    public void doForLightSensor(LightSensor lightSensor) {
        log.info("Sunset event has been applied to light sensor [{}].", lightSensor.getDeviceInteractionAPI().getName());
        for (Light light : lightSensor.getLights()) {
            log.info("Light [{}] has been turned on at sunset.", light.getDeviceInteractionAPI().getName());
            light.getDeviceInteractionAPI().setState(new IdleState());
        }
    }

    @Override
    public VisitedElement getRandomCompatibleElement() {
        return deviceRepository.getRandomDeviceOfType(DeviceType.LIGHT_SENSOR).getDeviceInteractionAPI();
    }
}
