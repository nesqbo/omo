package cz.cvut.fel.omo.smart_home.model.device.proxy;

import cz.cvut.fel.omo.smart_home.model.device.api.DataMeasureApi;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.model.factory.DeviceFactory;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static cz.cvut.fel.omo.smart_home.utils.UUIDUtil.generateUUID;

/**
 * Proxy class for handling device replacement during repair.
 */
@Component
@Slf4j
public class RepairProxy {
    private final DeviceRepository deviceRepository;
    private final AreaRepository areaRepository;
    private final DataMeasureApi dataMeasureApi;

    @Autowired
    public RepairProxy(DeviceRepository deviceRepository, AreaRepository areaRepository, DataMeasureApi dataMeasureApi) {
        this.deviceRepository = deviceRepository;
        this.areaRepository = areaRepository;
        this.dataMeasureApi = dataMeasureApi;
    }

    /**
     * Replaces a malfunctioning device with a new one.
     *
     * @param device The context of the malfunctioning device to be replaced.
     */
    public void replace(DeviceContext device) {
        Room room = areaRepository.getRoomByDeviceName(device.getDeviceInteractionAPI().getName());
        deviceRepository.removeDevice(device.getDeviceInteractionAPI().getName());
        room.removeDeviceByDeviceName(device.getDeviceInteractionAPI().getName());

        Device newDevice = DeviceFactory.createDevice(device.getDeviceType(), device.getDeviceInteractionAPI().getName() + generateUUID(), List.of(dataMeasureApi), this);
        log.info("Device [{}] is being replaced by [{}].", device.getDeviceInteractionAPI().getName(), newDevice.getDeviceInteractionAPI().getName());
        deviceRepository.addDevice(newDevice);
        room.addDevice(newDevice);
    }
}
