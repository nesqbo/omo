package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import org.springframework.stereotype.Component;

import java.util.List;

import static cz.cvut.fel.omo.smart_home.model.factory.AliveFactory.random;

/**
 * Enum representing different types of devices in the smart home system.
 *
 * <p>Each device type provides a factory method to create an instance of the corresponding device.
 */
@Component
public enum DeviceType {
    HEATER {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new Heater(name, subscribedApis, repairProxy);
        }
    },
    LIGHT {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new Light(name, subscribedApis, repairProxy);
        }
    },
    LIGHT_SENSOR {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new LightSensor(name, subscribedApis, repairProxy);
        }
    },
    OUTDOOR_BLINDS {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new OutdoorBlinds(name, subscribedApis, repairProxy);
        }
    },
    WIND_SENSOR {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new WindSensor(name, subscribedApis, repairProxy);
        }
    },
    CIRCUIT_BREAKER {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new CircuitBreaker(name, subscribedApis, repairProxy);
        }
    },
    GAS_STOVE {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new GasStove(name, subscribedApis, repairProxy);
        }
    },
    WASHING_MACHINE {
        @Override
        public Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
            return new WashingMachine(name, subscribedApis, repairProxy);
        }
    };

    public abstract Device createDevice(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy);

    public static DeviceType getRandomDeviceType() {
        return DeviceType.values()[random.nextInt(DeviceType.values().length)];
    }
}