package cz.cvut.fel.omo.smart_home.model.device.strategy;

import cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReplaceRepairStrategy implements RepairStrategy {
    @Override
    public void repair(DeviceContext context) {
        context.replace();
    }
}
