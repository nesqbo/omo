package cz.cvut.fel.omo.smart_home.model.device.strategy;

import cz.cvut.fel.omo.smart_home.model.deviceState.DeviceContext;

/**
 * Interface for strategies defining how devices are repaired.
 */
public interface RepairStrategy {

    /**
     * Repairs a device based on the provided context.
     *
     * @param context The context of the device to be repaired.
     */
    void repair(DeviceContext context);
}
