package cz.cvut.fel.omo.smart_home.model.device.api;

import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;

public interface Publisher {
    void notifySubscribers(CollectibleData data);
    void addSubscriber(Subscriber subscriber);
    void removeSubscriber(Subscriber subscriber);
}
