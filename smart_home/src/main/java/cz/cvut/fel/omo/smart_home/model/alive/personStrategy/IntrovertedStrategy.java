package cz.cvut.fel.omo.smart_home.model.alive.personStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.house.Area;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.house.Floor;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Scope("prototype")
public class IntrovertedStrategy extends BaseStrategy {
    public IntrovertedStrategy() {
    }

    @Override
    public void act(Alive person) {
        Area room = getCurrentArea(person);
        if (room.getPeople().size() >= 2) {
            log.info("IntrovertedStrategyPerson [{}]: I am not alone in this room [{}], I shall move to another room [{}]",person.getName(), room.getName(), findNextArea(person, room).getName());
        } else {
            log.info("IntrovertedStrategyPerson [{}]: I am alone in this room [{}], I shall stay here.", person.getName(), room.getName());
        }
    }

    @Override
    public void interactWith(Alive actingPerson, InteractibleEntity interactibleEntity) {
        if (getCurrentArea(actingPerson).getPeople().size() >=2){
            log.info("IntrovertedStrategyPerson [{}]: I do not enjoy being in other people's presence, I will move to room [{}]",actingPerson.getName(), findNextArea(actingPerson, getCurrentArea(actingPerson)).getName());
        } else {
            log.info("IntrovertedStrategyPerson [{}]: I am alone in this room [{}], I like it, I shall stay here.", actingPerson.getName(), getCurrentArea(actingPerson).getName());
        }
    }

    @Override
    public void beInteractedWith() {
        log.info("IntrovertedStrategyPerson : Please don't talk to me, I don't like it.");
    }

    private Area findNextArea(Alive person, Area room){
        Floor floor = room.getFloor();
        List<Room> areas = floor.getRooms();
        Area pickedArea = areas.get(0);

        for (Area r : areas){
            if (r.getPeople().isEmpty()){
                pickedArea = r;
                person.moveToArea(pickedArea);
                break;
            }
        }
        return pickedArea;
    }

}