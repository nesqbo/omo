package cz.cvut.fel.omo.smart_home.simulation.chain;

/**
 * The {@code Handler} interface defines the contract for handling requests in a chain of responsibility.
 */
public interface Handler {
    /**
     * Sets the next handler in the chain.
     *
     * @param handler The next handler in the chain.
     */
    void setNext(Handler handler);
    /**
     * Handles the request in the chain. If the handler is not capable of handling the request,
     * it may delegate the request to the next handler in the chain.
     */
    void handle();
}
