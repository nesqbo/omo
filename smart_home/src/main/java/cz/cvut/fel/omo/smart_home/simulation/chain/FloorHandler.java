package cz.cvut.fel.omo.smart_home.simulation.chain;

import cz.cvut.fel.omo.smart_home.model.house.Floor;
import cz.cvut.fel.omo.smart_home.model.house.House;
import cz.cvut.fel.omo.smart_home.utils.HouseConfigUtil;
import cz.cvut.fel.omo.smart_home.model.factory.FloorFactory;
import cz.cvut.fel.omo.smart_home.repositories.FloorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The {@code FloorHandler} class is responsible for handling the creation of floors in the smart home simulation.
 */
@Component
public class FloorHandler extends BaseHandler{
    private House house;
    private FloorRepository floorRepository;
    private HouseConfigUtil houseConfigUtil;
    @Autowired
    public FloorHandler(RoomHandler roomHandler, House house, FloorRepository floorRepository, HouseConfigUtil houseConfigUtil) {
        super(roomHandler);
        this.house = house;
        this.floorRepository = floorRepository;
        this.houseConfigUtil = houseConfigUtil;
    }

    public FloorHandler(Handler next) {
        super(next);
    }

    /**
     * Handles the creation of floors in the smart home simulation. Calls the next handler in the chain.
     */
    public void handle() {
        for (int i = 0; i < houseConfigUtil.floorCount; i++) {
            Floor floor = FloorFactory.createFloor(i);
            house.addFloor(floor);
            floorRepository.addFloor(floor);
        }
        handleNext();
    }
}
