package cz.cvut.fel.omo.smart_home.model.deviceState;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BrokenState extends DeviceState{
    public BrokenState() {
        super(0, 0, 0, 0);
        stateName = "Broken";
    }

    @Override
    public void changeToNextState(DeviceContext context) {
        //do nothing, stay broken
    }

}
