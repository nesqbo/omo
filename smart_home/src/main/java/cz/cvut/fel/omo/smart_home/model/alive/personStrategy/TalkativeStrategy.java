package cz.cvut.fel.omo.smart_home.model.alive.personStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.alive.Person;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.house.Room;

import cz.cvut.fel.omo.smart_home.model.house.Area;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
@Scope("prototype")
public class TalkativeStrategy extends BaseStrategy {
    @Override
    public void interactWith(Alive actingPerson, InteractibleEntity target) {
        if (getCurrentArea(actingPerson).getPeople().size() <= 1){
            log.info("TalkativePersonStrategy [{}] : Noo, I must move to another room as there is no one to talk to rn. Moving to room [{}].",actingPerson.getName(), findArea(actingPerson, getCurrentArea(actingPerson)).getName());
        } else {

            log.info("TalkativePersonStrategy [{}] : Hehehehehe time to talk to person [{}] in room [{}]",actingPerson.getName(), target.getName(), getCurrentArea(actingPerson).getName());
        }
    }

    @Override
    public void act(Alive actingPerson){
        Room room = (Room) getCurrentArea(actingPerson);

        if (room.getPeople().size() > 2) {
            Person target = null;
            for (Person person : room.getPeople()) {
                if (person != actingPerson) {
                    target = person;
                    break;
                }
            }
            assert target != null;
            log.info("TalkativePersonStrategy [{}]: I am interacting with [{}] in room [{}]", actingPerson.getName(), target.getName(), room.getName());
            target.beInteractedWith();
        } else if (!room.getAnimals().isEmpty()){
            log.info("TalkativePersonStrategy [{}]: I am interacting with [{}] in area [{}]", actingPerson.getName(), room.getAnimals().get(0).getName(), room.getName());
            room.getAnimals().get(0).beInteractedWith();
        }else {
            log.info("TalkativePersonStrategy [{}]: I have nobody to interact with, I will move to [{}] to search for other people", actingPerson.getName(), findArea(actingPerson, room).getName());
        }
    }

    @Override
    public void beInteractedWith() {
        log.info("Talkative person : I am being interacted with, I am talkative, I will talk to you.");
    }

    private Area findArea(Alive person, Area room){
        int roomsOnFloorCount = room.getFloor().getRooms().size();
        if (roomsOnFloorCount <= 1) {
            log.info("There is only [{}] on this floor so there is nowhere to run", room.getName());
            return room;
        }
        int randInt = new Random().nextInt(0, roomsOnFloorCount);
        while (randInt == room.getFloor().getAreas().indexOf(room)) {
            randInt = new Random().nextInt(0, roomsOnFloorCount);
        }
        Area area = room.getFloor().getAreas().get(randInt);
        person.moveToArea(area);
        return area;
    }

}