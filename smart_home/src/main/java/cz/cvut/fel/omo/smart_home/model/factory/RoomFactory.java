package cz.cvut.fel.omo.smart_home.model.factory;

import cz.cvut.fel.omo.smart_home.model.house.Floor;
import cz.cvut.fel.omo.smart_home.model.house.Room;

import java.util.Random;

/**
 * The {@code RoomFactory} class is responsible for creating instances of the {@code Room} class.
 */
public class RoomFactory {
    private static final Random random = new Random();
    private RoomFactory() {
    }

    /**
     * Creates a room on a given floor, adds room to floor
     * @param floor floor to add room to
     * @return created room
     */
    public static Room createRoom(Floor floor) {
        return new Room(floor, "Room"+ random.nextInt(1000));
    }
}
