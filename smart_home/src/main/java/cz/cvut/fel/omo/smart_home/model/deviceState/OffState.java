package cz.cvut.fel.omo.smart_home.model.deviceState;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OffState extends DeviceState {

    public OffState() {
        super(0, 0, 0, 0);
        stateName = "Off";
    }

    @Override
    public void changeToNextState(DeviceContext context) {
        //remain off
    }

}
