package cz.cvut.fel.omo.smart_home.model.house;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * The {@code House} class represents a smart home, consisting of multiple floors.
 */
@Slf4j
@Component
@Getter
public class House {
    private final HashMap<Integer, Floor> floors = new HashMap<>();

    /**
     * Adds a floor to the house.
     *
     * @param floor The floor to be added.
     */
    public void addFloor(Floor floor) {
        log.info("Adding floor [{}] to house", floor.getFloorNumber());
        floors.put(floor.getFloorNumber(), floor);
    }
}
