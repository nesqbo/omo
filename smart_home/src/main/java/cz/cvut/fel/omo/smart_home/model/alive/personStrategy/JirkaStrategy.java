package cz.cvut.fel.omo.smart_home.model.alive.personStrategy;

import cz.cvut.fel.omo.smart_home.model.alive.Alive;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.house.Area;
import cz.cvut.fel.omo.smart_home.model.house.Floor;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Slf4j
@Component
@Scope("prototype")
public class JirkaStrategy extends BaseStrategy{
    public JirkaStrategy() {
    }

    @Override
    public void act(Alive actingPerson) {
        Room room = (Room) getCurrentArea(actingPerson);
        if (room.getPeople().size() > 1 && !room.getDevices().isEmpty()) {
            Device interactWithDevice = room.getDevices().get(0);
            log.info("JirkaStrategyPerson [{}]: I am interacting with [{}] in room [{}]", actingPerson.getName(), interactWithDevice.getDeviceInteractionAPI().getName(), room.getName());
            interactWithDevice.getDeviceInteractionAPI().beInteractedWith();
        } else {
            Room nextRoomWithDevices = findNextRoomWithDevices(actingPerson, room);
            log.info("JirkaStrategyPerson [{}]: I have no devices to interact with, will move to room [{}] to search for other devices", actingPerson.getName(), nextRoomWithDevices.getName());
        }


    }

    @Override
    public void interactWith(Alive actingPerson, InteractibleEntity target) {
        List<Device> deviceList = getCurrentRoom(actingPerson).getDevices();
        if (deviceList.isEmpty()){
            log.info("JirkaStrategyPerson [{}] : Oh no, no devices, I shall move to the next room [{}]", actingPerson.getName(), findNextRoomWithDevices(actingPerson, getCurrentArea(actingPerson)));
        } else {
            Random random = new Random();
            int deviceNum = random.nextInt(deviceList.size());
            log.info("JirkaStrategyPerson [{}] : Oh, beautiful device [{}], I shall interact with you.",actingPerson.getName(), deviceList.get(deviceNum).getDeviceInteractionAPI().getName());
            target.beInteractedWith();
        }

    }

    @Override
    public void beInteractedWith() {
        log.info("JirkaStrategyPerson: Don't speak to me please, I only want to talk to devices.");
    }

    Room findNextRoomWithDevices(Alive person, Area room){
        Floor floor = room.getFloor();
        List<Room> rooms = floor.getRooms();
        Room pickedRoom = rooms.get(0);
        for (Room r : rooms){
            if (!r.getDevices().isEmpty()){
                pickedRoom = r;
                person.moveToRoom(pickedRoom);
                break;
            }
        }
        return pickedRoom;
    }
}
