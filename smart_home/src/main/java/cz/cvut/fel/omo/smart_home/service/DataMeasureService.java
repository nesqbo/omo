package cz.cvut.fel.omo.smart_home.service;

import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;
import cz.cvut.fel.omo.smart_home.repositories.DataMeasureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * The {@code DataMeasureService} class provides services related to device data measurement and reporting.
 */
@Service
public class DataMeasureService {
    @Autowired
    private DataMeasureRepository dataMeasureRepository;

    /**
     * Creates or updates the device report for the specified device name with the given data.
     *
     * @param deviceName The name of the device.
     * @param data       The collectible data to be associated with the device.
     */
    public void createOrUpdateDeviceData(String deviceName, CollectibleData data) {
        dataMeasureRepository.update(deviceName, data);
    }

    /**
     * Retrieves the collectible data for the specified device.
     *
     * @param deviceName The name of the device.
     * @return The collectible data associated with the device.
     */
    public CollectibleData getDataForDevice(String deviceName) {
        return dataMeasureRepository.getDataForDevice(deviceName);
    }

    /**
     * Retrieves all collected data for devices.
     *
     * @return A map containing device names as keys and their respective collectible data as values.
     */
    public HashMap<String, CollectibleData> getAllData() {
        return dataMeasureRepository.getAllData();
    }
}
