package cz.cvut.fel.omo.smart_home.model.event;

import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import cz.cvut.fel.omo.smart_home.repositories.AliveRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * A component responsible for generating events in the smart home system.
 */
@Slf4j
@Component
public class EventGenerator {
    private final DeviceRepository deviceRepository;

    private final AliveRepository aliveRepository;
    @Autowired
    public EventGenerator(DeviceRepository deviceRepository, AliveRepository aliveRepository) {
        this.deviceRepository = deviceRepository;
        this.aliveRepository = aliveRepository;
    }

    /**
     * Generates a visitor for the specified event type.
     *
     * @param eventType The type of the event.
     * @return A visitor corresponding to the specified event type.
     */
    public Visitor generateEvent(EventType eventType) {
        return switch (eventType) {
            case BREAK_DEVICE -> new BreakDeviceEvent(deviceRepository, aliveRepository);
            case SUNRISE -> new SunriseEvent(deviceRepository, aliveRepository);
            case SUNSET -> new SunsetEvent(deviceRepository, aliveRepository);
            case WEAK_WIND -> new WeakWindEvent(deviceRepository, aliveRepository);
            case STRONG_WIND -> new StrongWindEvent(deviceRepository, aliveRepository);
            case LOSS_OF_POWER -> new LossOfElectricityEvent(deviceRepository, aliveRepository);
        };
    }

    /**
     * Generates a random event.
     *
     * @return A visitor for a randomly selected event type.
     */
    public Visitor generateRandomEvent() {
        return generateEvent(
                EventType.getRandomEventType()
        );
    }

    /**
     * Enumeration representing various event types.
     */
    public enum EventType {
        BREAK_DEVICE,
        SUNRISE,
        SUNSET,
        WEAK_WIND,
        STRONG_WIND,
        LOSS_OF_POWER;


        /**
         * Gets a random event type.
         *
         * @return A randomly selected event type.
         */
        public static EventType getRandomEventType() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }
}
