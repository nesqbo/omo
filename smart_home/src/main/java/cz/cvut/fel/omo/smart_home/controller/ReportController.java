package cz.cvut.fel.omo.smart_home.controller;

import cz.cvut.fel.omo.smart_home.service.facade.ReportGeneratorFacade;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for handling report generation related endpoints.
 */
@RestController
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private ReportGeneratorFacade reportGeneratorFacade;
    /**
     * Generates a consumption report in JSON format.
     */
    @GetMapping("/consumption")
    public void generateJsonReport() {
        reportGeneratorFacade.generateConsumptionReport();
    }

    /**
     * Generates a JSON report for the specified device.
     *
     * @param deviceName The name of the device for which the report is generated.
     */
    @GetMapping("/{deviceName}")
    public void generateJsonReport(String deviceName) {
        reportGeneratorFacade.generateDeviceReport(deviceName);
    }

    /**
     * Generates a report for the entire house.
     */
    @GetMapping("/house")
    public void generateReportForHouse() {
        reportGeneratorFacade.generateHouseReport();
    }

    /**
     * Generates a JSON report for the entire house and writes it to a file.
     */
    @GetMapping("/json/house")
    public void generateJsonReportForHouse() {
        JSONObject report = reportGeneratorFacade.generateHouseReport();
        reportGeneratorFacade.writeToFile(report, ReportGeneratorFacade.ReportType.HOUSE);
    }

    /**
     * Generates an activity report for alives and returns it in JSON format.
     *
     * @return The activity report for alives in JSON format.
     */
    @GetMapping("/alives")//activity report
    public JSONObject generateReportForAlives() {
        return reportGeneratorFacade.generateAliveReport();
    }

    /**
     * Generates a JSON activity report for alives and writes it to a file.
     */
    @GetMapping("/json/alives")//activity report
    public void generateJsonReportForAlives() {
        JSONObject report = reportGeneratorFacade.generateAliveReport();
        reportGeneratorFacade.writeToFile(report, ReportGeneratorFacade.ReportType.ALIVES);
    }

}
