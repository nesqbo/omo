package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a wind sensor in the smart home system.
 */
@Getter
@Slf4j
public class WindSensor extends Device {
    private final List<OutdoorBlinds> outdoorBlinds = new ArrayList<>();

    /**
     * Creates a new instance of the WindSensor device with the specified parameters.
     *
     * @param name           The name of the wind sensor.
     * @param subscribedApis The list of data measure APIs to which the wind sensor subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public WindSensor(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.WIND_SENSOR, subscribedApis, repairProxy);
    }

    /**
     * Adds outdoor blinds to the list of outdoor blinds managed by the wind sensor.
     *
     * @param outdoorBlinds The outdoor blinds to add.
     */
    public void addOutdoorBlinds(OutdoorBlinds outdoorBlinds) {
        this.outdoorBlinds.add(outdoorBlinds);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForWindSensor(this);
    }
}
