package cz.cvut.fel.omo.smart_home.repositories;

import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * The {@code DeviceRepository} class manages the storage and retrieval of devices in the smart home.
 */
@Repository
@Slf4j
public class DeviceRepository {
    private final Random random = new Random();

    private final HashMap<String, Device> devices;

    public DeviceRepository() {
        devices = new HashMap<>();
    }

    /**
     * Adds a device to the repository.
     *
     * @param device The device to be added.
     * @throws IllegalArgumentException If a device with the same name already exists.
     */
    public void addDevice(Device device) {
        if (devices.containsKey(device.getDeviceInteractionAPI().getName())) {
            log.error("Device with name [[{}]] already exists", device.getDeviceInteractionAPI().getName());
            throw new IllegalArgumentException(
                    String.format("Device with name %s already exists", device.getDeviceInteractionAPI().getName()));
        }
        devices.put(device.getDeviceInteractionAPI().getName(), device);
    }

    /**
     * Retrieves a device from the repository based on the name.
     *
     * @param deviceName The name of the device to be retrieved.
     * @return The device with the specified name.
     * @throws IllegalArgumentException If no device with the specified name exists.
     */
    public Device getDeviceByDeviceName(String deviceName) {
        if (!devices.containsKey(deviceName)) {
            log.error("Device with name [[{}]] does not exist", deviceName);
            throw new IllegalArgumentException(
                    String.format("Device with name %s does not exist", deviceName));
        }
        return devices.get(deviceName);
    }

    /**
     * Returns a collection of all devices in the repository.
     *
     * @return The collection of devices.
     */
    public Collection<Device> getAllDevices() {
        return devices.values();
    }

    /**
     * Returns a list of all devices of a specific type in the repository.
     *
     * @param deviceType The type of devices to retrieve.
     * @return The list of devices of the specified type.
     * @throws IllegalArgumentException If the device type is null.
     */
    public List<Device> getAllDevicesOfType(DeviceType deviceType) {
        if (deviceType == null) {
            log.error("Device type cannot be null.");
            throw new IllegalArgumentException("Device type cannot be null.");
        }
        return devices
                .values()
                .stream()
                .filter(device -> device.getDeviceInteractionAPI().getDeviceType() == deviceType)
                .toList();
    }

    /**
     * Returns a randomly selected device from the repository.
     *
     * @return A randomly selected device.
     * @throws IllegalArgumentException If an issue occurs while finding a random device.
     */
    public Device getRandomDevice() {
        Optional<Device> device = getAllDevices().stream().skip(random.nextInt(devices.size())).findFirst();
        if (device.isPresent()){
            return device.get();
        } else {
            log.error("Issue finding random device.");
            throw new IllegalArgumentException("Issue finding random device.");
        }
    }

    /**
     * Returns a randomly selected device of a specific type from the repository.
     *
     * @param deviceType The type of device to retrieve.
     * @return A randomly selected device of the specified type.
     * @throws IllegalArgumentException If no devices of the specified type are found.
     */
    public Device getRandomDeviceOfType(DeviceType deviceType) {
        List<Device> devicesOfType = getAllDevicesOfType(deviceType);
        if (devicesOfType.isEmpty()) {
            log.error("No devices of type [{}] found.", deviceType);
            throw new IllegalArgumentException(String.format("No devices of type %s found.", deviceType));
        }
        return devicesOfType.get(random.nextInt(devicesOfType.size()));
    }

    /**
     * Removes a device from the repository based on the name.
     *
     * @param deviceName The name of the device to be removed.
     * @throws IllegalArgumentException If no device with the specified name exists.
     */
    public void removeDevice(String deviceName) {
        if (!devices.containsKey(deviceName)) {
            log.error("Device with name [[{}]] does not exist", deviceName);
            throw new IllegalArgumentException(
                    String.format("Device with name %s does not exist", deviceName));
        }
        devices.remove(deviceName);
    }
}
