package cz.cvut.fel.omo.smart_home;

import lombok.Getter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.beans.factory.annotation.Value;

/**
 * The {@code ApiConfig} class is a configuration class responsible for managing API-related configuration properties.
 * It is annotated with {@link Configuration} to indicate that it is a Spring configuration class.
 * The configuration properties are loaded from the "api.properties" file using the {@link PropertySource} annotation.
 * The class uses Lombok's {@link Getter} annotation to automatically generate getter methods for the properties.
 */
@Getter
@Configuration
@PropertySource("classpath:api.properties")
public class NameGeneratorApiConfig {

    @Value("${app.name-api.hostname}")
    private String hostname;

    @Value("${app.name-api.key}")
    private String key;
}