package cz.cvut.fel.omo.smart_home.utils;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * The {@code HouseConfigUtil} class is a utility class responsible for managing and validating the configuration properties related to the house simulation.
 * It is annotated with {@link Component} to indicate that it is a Spring bean and can be injected into other components.
 * The configuration properties are loaded from the "house.properties" file using the {@link PropertySource} annotation.
 * The class also contains validation logic in the {@link PostConstruct} method to ensure that the configuration values are valid.
 */
@Component
@PropertySource("classpath:house.properties")
public class HouseConfigUtil {
    @Value("${house.floor-count}")
    public int floorCount;

    @Value("${house.room-count}")
    public int roomCount;

    @Value("${house.device-count}")
    public int deviceCount;

    @Value("${house.person-count}")
    public int personCount;

    @Value("${house.animal-count}")
    public int animalCount;

    @Value("${house.outside-count}")
    public int outsideCount;

    /**
     * Validates the configuration values after the bean is constructed. Throws an {@link IllegalArgumentException} if any value is invalid.
     */
    @PostConstruct
    public void validate() {
        if (floorCount <= 0) {
            throw new IllegalArgumentException("Invalid floor count: " + floorCount);
        }

        if (roomCount <= 0) {
            throw new IllegalArgumentException("Invalid room count: " + roomCount);
        }

        if (personCount < 0) {
            throw new IllegalArgumentException("Invalid person count: " + personCount);
        }

        if (animalCount < 0) {
            throw new IllegalArgumentException("Invalid animal count: " + animalCount);
        }

        if (deviceCount < 0) {
            throw new IllegalArgumentException("Invalid device count: " + deviceCount);
        }

        if (outsideCount < 0) {
            throw new IllegalArgumentException("Invalid outside count: " + outsideCount);
        }
    }

}