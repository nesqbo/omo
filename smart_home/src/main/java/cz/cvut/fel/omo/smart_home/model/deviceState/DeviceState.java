package cz.cvut.fel.omo.smart_home.model.deviceState;

import lombok.Getter;

/**
 * Represents an abstract state of a device in the smart home system.
 */
@Getter
public abstract class DeviceState {
    protected String stateName;
    private final int electricityConsumption;
    private final int gasConsumption;
    private final int waterConsumption;
    private final int conditionDecrease;

    /**
     * Creates a new instance of the DeviceState with the specified parameters.
     *
     * @param electricityConsumption The electricity consumption associated with the device state.
     * @param gasConsumption         The gas consumption associated with the device state.
     * @param waterConsumption       The water consumption associated with the device state.
     * @param conditionDecrease      The condition decrease associated with the device state.
     */
    public DeviceState(int electricityConsumption, int gasConsumption, int waterConsumption, int conditionDecrease) {
        this.electricityConsumption = electricityConsumption;
        this.gasConsumption = gasConsumption;
        this.waterConsumption = waterConsumption;
        this.conditionDecrease = conditionDecrease;
    }

    /**
     * Abstract method to change the device to its next state.
     *
     * @param context The device context representing the device.
     */
    abstract public void changeToNextState(DeviceContext context);
}
