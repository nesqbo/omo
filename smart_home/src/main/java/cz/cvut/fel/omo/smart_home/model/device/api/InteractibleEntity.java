package cz.cvut.fel.omo.smart_home.model.device.api;

/**
 * An interface representing entities that can be interacted with in the smart home system.
 */
public interface InteractibleEntity {
    /**
     * Defines the behavior when the entity is interacted with.
     */
    void beInteractedWith();

    /**
     * Retrieves the name of the entity.
     *
     * @return The name of the entity.
     */
    String getName();
}
