package cz.cvut.fel.omo.smart_home.simulation.chain;

import cz.cvut.fel.omo.smart_home.model.device.api.DataMeasureApi;
import cz.cvut.fel.omo.smart_home.model.device.DeviceType;
import cz.cvut.fel.omo.smart_home.model.device.Device;
import cz.cvut.fel.omo.smart_home.model.device.Light;
import cz.cvut.fel.omo.smart_home.model.device.LightSensor;
import cz.cvut.fel.omo.smart_home.model.device.OutdoorBlinds;
import cz.cvut.fel.omo.smart_home.model.device.WindSensor;
import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.utils.HouseConfigUtil;
import cz.cvut.fel.omo.smart_home.model.factory.DeviceFactory;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import cz.cvut.fel.omo.smart_home.repositories.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.smart_home.utils.UUIDUtil.generateUUID;

/**
 * The {@code DeviceHandler} class is responsible for handling the creation of devices in the smart home simulation.
 */
@Component
@Slf4j
public class DeviceHandler extends BaseHandler {
    private final DeviceRepository deviceRepository;
    private final AreaRepository areaRepository;
    private final HouseConfigUtil houseConfigUtil;
    private final List<Subscriber> subscribedApis = new ArrayList<>();
    private final RepairProxy repairProxy;

    /**
     * Constructs a {@code DeviceHandler} with dependencies.
     *
     * @param aliveHandler    The next handler in the chain.
     * @param deviceRepository The repository for devices.
     * @param areaRepository   The repository for rooms in the smart home.
     * @param houseConfigUtil  The utility class for house configuration.
     * @param repairProxy      The repair proxy for devices.
     * @param dataMeasureApi   The API for data measurement.
     */
    @Autowired
    public DeviceHandler(AliveHandler aliveHandler, DeviceRepository deviceRepository, AreaRepository areaRepository,
                         HouseConfigUtil houseConfigUtil, RepairProxy repairProxy, DataMeasureApi dataMeasureApi) {
        super(aliveHandler);
        this.deviceRepository = deviceRepository;
        this.areaRepository = areaRepository;
        this.houseConfigUtil = houseConfigUtil;
        this.repairProxy = repairProxy;
        subscribedApis.add(dataMeasureApi);
    }

    @Override
    public void handle() {
        int deviceTypeCount = DeviceType.values().length;
        if (houseConfigUtil.deviceCount < deviceTypeCount) {
            log.error("House must contain [{}] devices at least for the simulation to run properly.", deviceTypeCount);
            throw new IllegalArgumentException("House must contain " + deviceTypeCount + " devices at least for the simulation to run properly.");
        }

        createEachDeviceTypeOnce();
        createRandomDevices(houseConfigUtil.deviceCount - deviceTypeCount);
        handleNext();
    }

    /**
     * Creates one device of each type and adds it to a randomly selected room in the smart home simulation.
     */
    private void createEachDeviceTypeOnce() {
        for (DeviceType deviceType : DeviceType.values()) {
            Room room = areaRepository.getRandomRoom();
            Device device = DeviceFactory.createDevice(deviceType, deviceType.toString() + generateUUID(), subscribedApis,
                    repairProxy);
            room.addDevice(device);

            specialHandle(device, room);

            deviceRepository.addDevice(device);
        }
    }

    /**
     * Creates random devices of random types and adds them to randomly selected rooms in the smart home simulation.
     *
     * @param count The number of devices to create.
     */
    private void createRandomDevices(int count) {
        for (int i = 0; i < count; i++) {
            Room room = areaRepository.getRandomRoom();
            DeviceType deviceType = DeviceType.getRandomDeviceType();
            Device device = DeviceFactory.createDevice(deviceType, deviceType.toString() + generateUUID(), subscribedApis, repairProxy);
            room.addDevice(device);

            specialHandle(device, room);

            deviceRepository.addDevice(device);
        }
    }

    /**
     * Adds lights to a room with a light sensor.
     *
     * @param room       The room with the light sensor.
     * @param lightSensor The light sensor.
     */
    private void addLightsToRoomWithSensor(Room room, LightSensor lightSensor) {
        List<Light> lights = new ArrayList<>();

        room.getDevices()
                .stream()
                .filter(device -> device.getDeviceInteractionAPI().getDeviceType().equals(DeviceType.LIGHT))
                .forEach(device -> lights.add((Light) device));

        for (int j = 0; j < 3; j++) {
            Light light = DeviceFactory.createDevice(DeviceType.LIGHT, DeviceType.LIGHT + generateUUID(), subscribedApis, repairProxy);
            lights.add(light);
            room.addDevice(light);
        }
        lightSensor.addLights(lights);
    }

    /**
     * Adds a light to all light sensors in a room.
     *
     * @param room The room with the light sensors.
     * @param light The light to add.
     */
    private void addLightToSensorsInTheRoom(Room room, Light light) {
        room.getDevices()
                .stream()
                .filter(device -> device.getDeviceInteractionAPI().getDeviceType().equals(DeviceType.LIGHT_SENSOR))
                .forEach(device -> ((LightSensor) device).addLights(List.of(light)));
    }

    /**
     * Adds existing blinds to a wind sensor.
     *
     * @param room       The room with the wind sensor.
     * @param windSensor The wind sensor.
     */
    private void addExistingBlindsToWindSensor(Room room, WindSensor windSensor) {
        room.getDevices()
                .stream()
                .filter(device -> device.getDeviceInteractionAPI().getDeviceType().equals(DeviceType.OUTDOOR_BLINDS))
                .map(device -> ((OutdoorBlinds) device))
                .forEach(windSensor::addOutdoorBlinds);

        if (windSensor.getOutdoorBlinds().isEmpty()) {
            OutdoorBlinds outdoorBlinds = DeviceFactory.createDevice(DeviceType.OUTDOOR_BLINDS, DeviceType.OUTDOOR_BLINDS + generateUUID(), subscribedApis, repairProxy);
            room.addDevice(outdoorBlinds);
            windSensor.addOutdoorBlinds(outdoorBlinds);
        }
    }

    /**
     * Adds blinds to a room with a wind sensor.
     *
     * @param room           The room with the wind sensor.
     * @param outdoorBlinds The outdoor blinds.
     */
    private void addBlindsToRoomWithSensor(Room room, OutdoorBlinds outdoorBlinds) {
        room.getDevices()
                .stream()
                .filter(device -> device.getDeviceInteractionAPI().getDeviceType().equals(DeviceType.WIND_SENSOR))
                .map(device -> ((WindSensor) device))
                .forEach(windSensor -> windSensor.addOutdoorBlinds(outdoorBlinds));
    }

    /**
     * Handles special cases for certain devices in the smart home simulation.
     *
     * @param device The device to handle.
     * @param room   The room where the device is located.
     */
    private void specialHandle(Device device, Room room) {
        switch (device.getDeviceInteractionAPI().getDeviceType()) {
            case LIGHT_SENSOR:
                addLightsToRoomWithSensor(room, (LightSensor) device);
                break;
            case LIGHT:
                addLightToSensorsInTheRoom(room, (Light) device);
                break;
            case WIND_SENSOR:
                addExistingBlindsToWindSensor(room, (WindSensor) device);
                break;
            case OUTDOOR_BLINDS:
                addBlindsToRoomWithSensor(room, (OutdoorBlinds) device);
                break;
            default:
                break;
        }
    }
}
