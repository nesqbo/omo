package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Represents outdoor blinds in the smart home system.
 */
@Slf4j
public class OutdoorBlinds extends Device {

    /**
     * Creates a new instance of the OutdoorBlinds device with the specified parameters.
     *
     * @param name           The name of the outdoor blinds.
     * @param subscribedApis The list of data measure APIs to which the outdoor blinds subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public OutdoorBlinds(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.OUTDOOR_BLINDS, subscribedApis, repairProxy);
        log.info("Outdoor blinds [{}] have been created.", name);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForOutdoorBlinds(this);
    }
}
