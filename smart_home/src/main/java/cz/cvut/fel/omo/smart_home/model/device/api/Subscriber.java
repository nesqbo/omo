package cz.cvut.fel.omo.smart_home.model.device.api;

import cz.cvut.fel.omo.smart_home.model.device.collectibleData.CollectibleData;

/**
 * Interface for APIs measuring data from devices.
 * Implements a Observer design pattern
 */
public interface Subscriber {

    //pokazde co udelam update, tak ten danny list mi vraci informace o tomto jednotlivem, nebo klidne o vsech
    /**
     * Method to update stored data in API upon receiving notification from device.
     */
    void update(String deviceName, CollectibleData data);
}
