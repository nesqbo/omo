package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState;
import cz.cvut.fel.omo.smart_home.model.deviceState.IdleState;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Represents a washing machine in the smart home system.
 */
@Slf4j
public class WashingMachine extends Device {

    /**
     * The number of washing cycles completed.
     */
    private int washingCycle = 0;

    /**
     * The number of cycles required to finish washing.
     */
    private final int cyclesRequiredToFinish = 3;

    /**
     * Creates a new instance of the WashingMachine device with the specified parameters.
     *
     * @param name           The name of the washing machine.
     * @param subscribedApis The list of data measure APIs to which the washing machine subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public WashingMachine(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.WASHING_MACHINE, subscribedApis, repairProxy);
        setIdleState(new IdleState(10, 0, 10, 10));
        setActiveState(new ActiveState(50, 0, 30, 15));
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForWashingMachine(this);
    }

    @Override
    public void act() {
        if (isActive()) {
            log.info("Washing machine [{}] is washing clothes.", deviceName);
            washingCycle++;
        }
        if (washingCycle >= cyclesRequiredToFinish) {
            log.info("Washing machine [{}] finished washing clothes.", deviceName);
            washingCycle = 0;
            state.changeToNextState(this.getDeviceInteractionAPI());
        }
        increaseElectricityConsumption(state.getElectricityConsumption());
        increaseWaterConsumption(state.getWaterConsumption());
        decreaseCondition(state.getConditionDecrease());
    }
}
