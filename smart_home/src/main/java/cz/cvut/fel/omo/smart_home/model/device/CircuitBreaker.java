package cz.cvut.fel.omo.smart_home.model.device;

import cz.cvut.fel.omo.smart_home.model.device.api.Subscriber;
import cz.cvut.fel.omo.smart_home.model.deviceState.ActiveState;
import cz.cvut.fel.omo.smart_home.model.device.proxy.RepairProxy;
import cz.cvut.fel.omo.smart_home.model.device.strategy.ReplaceRepairStrategy;
import cz.cvut.fel.omo.smart_home.model.event.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Represents a circuit breaker device in the smart home system.
 * Extends the base {@link Device} class.
 *
 * <p>This class provides specific behavior for a circuit breaker device, including
 * the actuation logic and acceptance of a Visitor.
 */
@Slf4j
public class CircuitBreaker extends Device {

    /**
     * Initiates the actuation of the circuit breaker.
     * Increases electricity consumption and decreases condition.
     */
    public void act() {
        increaseElectricityConsumption(1);
        decreaseCondition(1);
    }

    /**
     * Constructs a CircuitBreaker object with the given parameters.
     *
     * @param name           The name of the circuit breaker.
     * @param subscribedApis The list of data measure APIs to which the circuit breaker subscribes.
     * @param repairProxy    The repair proxy for handling repairs and replacements.
     */
    public CircuitBreaker(String name, List<Subscriber> subscribedApis, RepairProxy repairProxy) {
        super(name, DeviceType.CIRCUIT_BREAKER, subscribedApis, repairProxy);
        this.getDeviceInteractionAPI().setState(new ActiveState());
        setRepairStrategy(new ReplaceRepairStrategy());
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.doForCircuitBreaker(this);
    }
}
