package cz.cvut.fel.omo.smart_home.simulation.chain;

import cz.cvut.fel.omo.smart_home.model.house.Floor;
import cz.cvut.fel.omo.smart_home.utils.HouseConfigUtil;
import cz.cvut.fel.omo.smart_home.repositories.AreaRepository;
import cz.cvut.fel.omo.smart_home.repositories.FloorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The {@code OutsideHandler} class is responsible for handling the creation of outside areas in the smart home simulation.
 */
@Component
@Slf4j
public class OutsideHandler extends BaseHandler{

    private final AreaRepository outsideRepository;
    private final HouseConfigUtil houseConfigUtil;
    private final FloorRepository floorRepository;

    @Autowired

    public OutsideHandler(AreaRepository outsideRepository, HouseConfigUtil houseConfigUtil, FloorRepository floorRepository) {
        super(null);
        this.outsideRepository = outsideRepository;
        this.houseConfigUtil = houseConfigUtil;
        this.floorRepository = floorRepository;
    }


    @Override
    public void handle() {
        Floor floor = floorRepository.getFloor(0);
        for (int i = 0; i < houseConfigUtil.outsideCount; i++) {
            String outsideType = String.valueOf(outsideTypes.values()[i % outsideTypes.values().length]);
            outsideRepository.createOutside(floor, outsideType);
        }
        handleNext();
    }

    /**
     * Enum representing different types of outside areas.
     */
    enum outsideTypes{
        GARDEN,
        TERRACE,
        NEIGHBOURS_HOUSE,
        STREET,
        PARK,
        FOREST,
        LAKE,
        RIVER,
        TOWN,
        HILL,
        MOUNTAIN,
        BEACH
    }
}
