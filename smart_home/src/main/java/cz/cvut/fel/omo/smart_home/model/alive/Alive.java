package cz.cvut.fel.omo.smart_home.model.alive;

import cz.cvut.fel.omo.smart_home.model.house.Area;
import cz.cvut.fel.omo.smart_home.model.house.Outside;
import cz.cvut.fel.omo.smart_home.model.device.api.InteractibleEntity;
import cz.cvut.fel.omo.smart_home.model.house.Room;
import cz.cvut.fel.omo.smart_home.simulation.SimulationActor;

/**
 * Interface representing a living entity in the smart home system.
 */
public interface Alive extends InteractibleEntity, SimulationActor {
    /**
     * Moves the alive entity to the specified room.
     *
     * @param room The target room for the move.
     */
    void moveToRoom(Room room);

    /**
     * Moves the alive entity to the specified area.
     *
     * @param area The target area for the move.
     */
    void moveToArea(Area area);

    /**
     * Moves the alive entity to the outside area.
     *
     * @param outside The outside area for the move.
     */
    void moveToOutside(Outside outside);

    /**
     * Initiates an interaction with the specified interactible entity.
     *
     * @param interactibleEntity The entity to be interacted with.
     */
    void interactWith(InteractibleEntity interactibleEntity);

    String getName();


    enum AliveType {
        PERSON,
        ANIMAL
    }
}
